import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class LoadingComponent implements OnInit {

  constructor(public general: GeneralService) { }
  @Input('inlineLoading') inlineLoading = false;
  ngOnInit() {
  }

}
