import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class NavigationComponent implements OnInit {

  constructor(
    private auth: AuthenticationService,
  ) { }
  @ViewChild('checkbox', {static: false}) checkbox;
  ngOnInit() {
  }
  hasSid(){
    return sessionStorage.getItem('sid');
  }
  loggedIn(){
    return this.auth.IS_AUTHENTICATED
  }
  logout(){
    this.checkbox.nativeElement.checked=false
    this.auth.LOGOUT()
  }
}
