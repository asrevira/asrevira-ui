import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private modalService: ModalService,
    private auth: AuthenticationService,
  ) { }
  userInfo = null;
  ewallet = null;
  loading = false;
  ngOnInit() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000);
    this.auth.USER_INFO_OBSERVABLE.subscribe(
      (res: Response) => {
        this.userInfo = res
      }
    )
    this.auth.USER_EWALLET_OBSERVABLE.subscribe(
      (res: Response) => {
        this.ewallet = res
      }
    )
  }

  getDefaultProfile(){
    switch(this.userInfo.gender){
      case '0':
        return '../../../assets/0.png';
      case '1':
        return '../../../assets/1.png';
      case '2':
        return '../../../assets/2.png';
      default:
        return '../../../assets/0.png'
    }
  }

  hasSid(){
    return sessionStorage.getItem('sid')
  }

  logout(){
    this.auth.LOGOUT()
  }
  
  isAuthenticated(){
    return this.auth.IS_AUTHENTICATED;
  }
  openModal(modal){
    this.modalService.openModal(modal)
  }
}
