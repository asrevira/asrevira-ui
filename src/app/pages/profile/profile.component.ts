import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { GeneralService } from 'src/app/services/general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private GeneralService: GeneralService,
    private router: Router
  ) { }
  profile = null
  ewallet = null
  ngOnInit() {
    if(!localStorage.getItem('access_user')){
      this.router.navigate(['ورود'])
    }
    try{
      this.auth.GET_USER_EWALLET()
      this.auth.GET_USER_INFO()
    }
    catch{
      this.router.navigate(['ورود'])
    }
    this.auth.USER_INFO_OBSERVABLE.subscribe(
      (res: Response) => {
        this.profile = res;
        if(this.ewallet){
          setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
        }
      }
    )
    this.auth.USER_EWALLET_OBSERVABLE.subscribe(
      (res: Response) => {
        this.ewallet = res;
        if(this.profile){
          setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
        }
      }
    )
  }
  getStatus(code){
    switch(code){
      case 0:
        return 'غیر فعال';
      case 1:
        return 'فعال';
      case 2:
        return 'غیر فعال';
      case 3:
        return 'بازداشت شده';
      case 4:
        return 'بدهی سیستمی';
      case 5:
        return 'غیر فعال سیستمی';
    }
  }
  getGender(){
    switch(this.profile.gender){
      case 0:
        return 'نامشخص';
      case 1:
        return 'مرد';
      case 2:
        return 'زن';
      default:
        return 'نامشخص'
    }
  }

}
