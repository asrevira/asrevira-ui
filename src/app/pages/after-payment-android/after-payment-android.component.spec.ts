import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfterPaymentAndroidComponent } from './after-payment-android.component';

describe('AfterPaymentAndroidComponent', () => {
  let component: AfterPaymentAndroidComponent;
  let fixture: ComponentFixture<AfterPaymentAndroidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfterPaymentAndroidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfterPaymentAndroidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
