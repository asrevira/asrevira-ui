import { Component, OnInit } from '@angular/core';
import { TransactionsService } from 'src/app/services/transactions.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Response } from 'src/app/dto/response.model';
import { addToTransactions } from 'src/app/utils/helper';

@Component({
  selector: 'app-after-payment',
  templateUrl: './after-payment.component.html',
  styleUrls: ['./after-payment.component.scss']
})
export class AfterPaymentComponent implements OnInit {

  constructor(
    private transactions: TransactionsService,
    private activatedRoute: ActivatedRoute,
    private GeneralService: GeneralService,
    private auth: AuthenticationService,
    private router: Router
  ) {}
  transaction = null;
  subscription;
  loading = false;
  async ngOnInit() {
    this.GeneralService.loading = true;
    const snapshot = this.activatedRoute.snapshot;
    const invoice = snapshot.paramMap.get('invoice');
    this.loading = true;
    this.auth.PUBLIC_TOKEN_OBSERVABLE.subscribe(
      () => {
        this.subscription = this.transactions.GET_TRANSACTION(invoice, 'PUBLIC').subscribe(
          (res: Response) => {
            this.transaction = res.ASREVIRA['payments']
            if(!this.auth.IS_AUTHENTICATED){
              addToTransactions(this.transaction)
            }
            this.GeneralService.loading = false;
            this.loading = false;
            this.subscription.unsubscribe()
            // if(!this.transaction.length){
            //   this.router.navigate(['شارژ'])
            // }
    
          },
          err => {
            this.GeneralService.loading = false;
            this.loading = false;
            this.router.navigate(['']);
            this.subscription.unsubscribe()
          }
        )
      }
    )
    
    
    
    
  }
  copyMessage(){
    this.GeneralService.openSnackBar(`کد شارژ مورد نظر کپی شد.`, 'بستن', 3000)
  }
  getTransactionName(){
    if(this.transaction.type == 11){
      return 'خرید شارژ مستقیم';
    }
    if(this.transaction.type == 12){
      return 'خرید بسته اینترنت';
    }
    if(this.transaction.type == 13){
      return 'خرید کارت شارژ';
    }
    return;
  }
  getOperatorByCode(code){
    if(code == 2){
      return {description: 'رایتل', value: 'Rightel'}
    }else if(code == 0){
      return {description: 'ایرانسل', value: 'Mtn'}
    }else if(code == 1){
      return {description: 'همراه‌اول', value: 'Mci'}
    }
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe()
  }
}
