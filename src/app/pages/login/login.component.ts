import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { clearFlatObject } from 'src/app/utils/helper';
import { GeneralService } from 'src/app/services/general.service';
import { BundleService } from 'src/app/services/bundle.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private GeneralService: GeneralService,
    private bundle: BundleService,
    private router: Router

  ) { }
  username = '';
  password = '';
  loading = false;
  messages = {
    general: '',
    username: '',
    password: '',
  }
  ngOnInit() {
    setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
    if(this.auth.IS_AUTHENTICATED){
      this.router.navigate(['/'])
    }
  }

  async login(){
    if(!this.username){
      this.messages.username = 'لطفا نام کاربری خود را وارد کنید.'
    }
    else if(!this.password){
      this.messages.username = '';
      this.messages.password = 'لطفا رمز عبور خود را وارد کنید.'
    }
    else if(this.password.length < 4){
      this.messages.username = '';
      this.messages.password = 'رمز عبور وارد شده کمتر از ۴ حرف است.'
    }
    else {
      const loginObj = {
        auth: this.username,
        pwd: this.password
      }
      this.messages = clearFlatObject(this.messages)
      this.loading = true;
      try{
        await this.bundle.normalLogin(loginObj)
        this.loading = false;
      }catch{
        this.loading = false;
      }
    }
  }
}
