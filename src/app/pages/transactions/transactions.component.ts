import { Component, OnInit } from '@angular/core';
import { TransactionsService } from 'src/app/services/transactions.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { GeneralService } from 'src/app/services/general.service';
import { transition } from '@angular/animations';
import { addToFavoritePhoneNumber, getDiscount, detectOperator, getSimNamePersian, getFavoritePhoneNumber, getTransactions } from 'src/app/utils/helper';
import { ModalService } from 'src/app/services/modal.service';
import { Response } from 'src/app/dto/response.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  constructor(
    private transactions: TransactionsService,
    private GeneralService: GeneralService,
    private auth: AuthenticationService,
    private ModalService: ModalService,
    private router: Router
  ) { }
  status = 'all';
  loading = false;
  confirmedStatus = 'all'
  type = 'all'
  confirmedType = 'all'
  transactionsList = [];
  subscription;
  background = false;
  ngOnInit() {
    if(!this.auth.IS_AUTHENTICATED){
      this.transactionsList = getTransactions()
      this.transactions.USER_TRANSACTIONS = getTransactions()
      this.subscription = this.auth.IS_AUTHENTICATED_OBSERVABLE.subscribe(
        (isLoggedin) => {
          this.GeneralService.loading = true;
          if(isLoggedin){
            this.transactions.GET_USER_TRANSACTIONS().subscribe(
              (res: Response) => {
                this.transactions.USER_TRANSACTIONS = res.ASREVIRA['payments'];
                this.transactionsList = this.transactions.USER_TRANSACTIONS;
                setTimeout(() => {
                  this.GeneralService.loading = false;
                }, 1000);
              } 
            )
            this.subscription.unsubscribe()
          }
        }
      )
    }else{
      this.GeneralService.loading = true;
      this.transactions.GET_USER_TRANSACTIONS().subscribe(
        (res: Response) => {
          this.transactions.USER_TRANSACTIONS = res.ASREVIRA['payments'];
          this.transactionsList = this.transactions.USER_TRANSACTIONS;
          setTimeout(() => {
            this.GeneralService.loading = false;
          }, 1000);
        } 
      )
    }
    
    
  }

  getTransactionTitle(transaction){
    switch(transaction.type){
      case '11':
        return 'شارژ مستقیم' 
      case '12':
        return 'بسته‌اینترنت'
      case '13':
        return 'کارت شارژ'
    }
  }

  inTheList(phone){
    return getFavoritePhoneNumber().find(num => num == phone) || false;
  }

  doTransactionAgain(transaction){
    if(transaction.type == '12'){
      const {req_amount, req_number, req_bundle_id, req_bundle_title} = transaction.pattern;
      const newTransaction = {
        type: 'internet',
        package_name: req_bundle_title,
        price: req_amount,
        finalPrice: req_amount,
        discount: getDiscount(11, this.getOperator(req_number), req_amount),
        data: {
          gateway_payment: 10,
          bundle_id: req_bundle_id,
          number: req_number,
          op: this.getOperator(req_number),
          simtype: 1,
          request_id: null,
        }
      }
      this.ModalService.openModal('invoice', newTransaction)
    }
    if(transaction.type == '11'){
      const {req_amount, req_number, req_simtype, req_paybill, req_profile} = transaction.pattern;
      const newTransaction = {
        type: 'topup',
        function: null,
        discount: getDiscount(12, this.getOperator(req_number), req_amount),
        data: {
          gateway_payment: 10,
          amount: req_amount * 10,
          number: req_number,
          op: this.getOperator(req_number),
          simtype: req_simtype,
          profile: req_profile.toLowerCase(),
          paybill: req_paybill,
        }
      }
      this.ModalService.openModal('invoice', newTransaction)
    }
    if(transaction.type == '13'){
      const {req_isid, req_vtpcode} = transaction.pattern;
      const newTransaction = {
        type: 'chargeCode',
        function: null,
        discount: getDiscount(13, this.getOperator(req_isid), transaction.amount),
        amount: transaction.amount,
        data: {
          gateway_payment: 10,
          isid: req_isid,
          vtpcode: req_vtpcode,
          count: 1,
        }
      }
      this.ModalService.openModal('invoice', newTransaction)
    }
    
  }

  showDetails(transaction){
    if(transaction.type == '12'){
      const {req_amount, req_number, req_bundle_id, req_bundle_title} = transaction.pattern;
      const newTransaction = {
        type: 'internet',
        package_name: req_bundle_title,
        price: req_amount,
        finalPrice: req_amount,
        discount: getDiscount(11, this.getOperator(req_number), req_amount),
        data: {
          gateway_payment: 10,
          bundle_id: req_bundle_id,
          number: req_number,
          op: this.getOperator(req_number),
          simtype: 0,
          request_id: null,
        }
      }
      this.ModalService.openModal('factor', newTransaction)
    }
    if(transaction.type == '11'){
      const {req_amount, req_number, req_simtype, req_paybill, req_profile} = transaction.pattern;
      const newTransaction = {
        type: 'topup',
        function: null,
        discount: getDiscount(12, this.getOperator(req_number), req_amount),
        data: {
          gateway_payment: 10,
          amount: req_amount * 10,
          number: req_number,
          op: this.getOperator(req_number),
          simtype: req_simtype,
          profile: req_profile.toLowerCase(),
          paybill: req_paybill,
        }
      }
      this.ModalService.openModal('factor', newTransaction)
    }
    if(transaction.type == '13'){
      const {req_isid, req_vtpcode, req_simtype, req_paybill, req_profile} = transaction.pattern;
      const newTransaction = {
        type: 'chargeCode',
        function: null,
        discount: getDiscount(13, this.getOperator(req_isid), transaction.amount),
        amount: transaction.amount,
        data: {
          gateway_payment: 10,
          isid: req_isid,
          vtpcode: req_vtpcode,
          count: 1,
        }
      }
      this.ModalService.openModal('factor', newTransaction)
    }
    
  }

  getOperator(phoneNumber){
    return detectOperator(phoneNumber).code
  }

  addFavoriteNumber(phoneNumber){
    addToFavoritePhoneNumber(phoneNumber)
    this.GeneralService.openSnackBar(`شماره ${phoneNumber} به شماره های منتخب اضافه شد.`, 'بستن', 5000)
  }
  copyMessage(){
    this.GeneralService.openSnackBar(`کد پیگیری تراکنش کپی شد.`, 'بستن', 3000)
  }

  filterType(type){
    this.type = type;
  }
  filter(){
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.confirmedStatus = this.status
      this.confirmedType = this.type
    }, 1000);
    
  }
  toggleBackground(){
    this.background = !this.background;
  }
  getTransactionsByStatus(status, transactionsList){
    if(status == 'success'){
      return transactionsList.filter(transaction => transaction['status'] == 1 && transaction['verify'] == 1)
    }else if(status == 'failure'){
      return transactionsList.filter(transaction => transaction['status'] == 0 || transaction['verify'] == 0)
    }
    return transactionsList;
  }
  getTransactionsByType(type, transactionsList){
    if(type == 'all'){
      return transactionsList;
    }
    return transactionsList.filter(transaction => transaction['type'] == type)
  }
  getTransactionsList(){
    let transactionsList = [...this.transactionsList];
    transactionsList = this.getTransactionsByStatus(this.confirmedStatus, transactionsList)
    transactionsList = this.getTransactionsByType(this.confirmedType, transactionsList)
    return transactionsList;
  }

  getTransactionTypeText(){
    const transactionsTypes = [
      {value: 'all', description: 'همه'},
      {value: '11', description: 'شارژ'},
      {value: '12', description: 'بسته اینترنت'},
    ]
    return transactionsTypes.find(type => type.value == this.type).description
  }
  getOperatorByCode(code){
    if(code == 2){
      return {description: 'رایتل', value: 'Rightel'}
    }else if(code == 0){
      return {description: 'ایرانسل', value: 'Mtn'}
    }else if(code == 1){
      return {description: 'همراه‌اول', value: 'Mci'}
    }
  }
}
