import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopupComponent } from './products/topup/topup.component';
import { InternetPackagesComponent } from './products/internet-packages/internet-packages.component';
import { InternetPackagesResultsComponent } from './products/internet-packages/internet-packages-results/internet-packages-results.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './pages/login/login.component';
import { TopupTrackingComponent } from './products/topup/topup-tracking/topup-tracking.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AfterPaymentComponent } from './pages/after-payment/after-payment.component';
import { ChargeCodeComponent } from './products/charge-code/charge-code.component';
import { AfterPaymentAndroidComponent } from './pages/after-payment-android/after-payment-android.component';


const routes: Routes = [
  {path: 'payment/android/:invoice', component: AfterPaymentAndroidComponent},
  {path: 'payment/:invoice', component: AfterPaymentComponent},
  {path: 'ورود', component: LoginComponent},
  {path: 'تراکنش‌ها', component: TransactionsComponent},
  {path: 'پروفایل', component: ProfileComponent},
  {path: '', component: ProductsComponent, children: [
    {
      path: '',
      children: [
        {
          path: '',
          component: TopupComponent,
        },
        {
          path: 'پیگیری',
          component: TopupTrackingComponent,
        }
      ],
    },
    {
      path: 'شارژ',
      children: [
        {
          path: '',
          component: TopupComponent,
        },
        {
          path: 'پیگیری',
          component: TopupTrackingComponent,
        }
      ],
    },
    {
      path: 'کارت-شارژ',
      children: [
        {
          path: '',
          component: ChargeCodeComponent,
        }
      ],
    },
    {
      path: 'اینترنت',
      children: [
        {
          path: '',
          component: InternetPackagesComponent,
        },
        {
          path: 'بسته‌ها',
          component: InternetPackagesResultsComponent,
        },
      ]
    }
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
