import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { slideInAnimation, loading } from './utils/animation';
import { RouterOutlet, RouterEvent, NavigationStart, NavigationCancel, NavigationError, Router, ActivatedRoute } from '@angular/router';
import { getCookie, getQueryStringValue, getPathFromUrl } from './utils/helper';
import { GeneralService } from './services/general.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ModalService } from './services/modal.service';
import { BundleService } from './services/bundle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation,
    trigger('loading',[
      state('*',style({
        'opacity': '1'
      })),
      transition('void => *',[style({
        'opacity': '0'
      })]),
      transition('* => void',[animate(200,style({
          'opacity': '0'
        }))
      ])
    ])
    // animation triggers go here
  ]
})
export class AppComponent {
  title = 'asrevira';
  loading;
  constructor(
    private auth: AuthenticationService,
    private bundle: BundleService,
    public GeneralService: GeneralService,
    private Router: Router,
    private ActivatedRoute: ActivatedRoute,
    private modal: ModalService
  ) {
    this.Router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    
  }
  async initial(){
    
  }
  isFromAndroid(){
    return window.location.pathname.search('android') > -1
  }
  async ngOnInit() {
    if(this.isFromAndroid()){
      await this.bundle.callPublicToken();
      this.auth.PUBLIC_TOKEN_OBSERVABLE.emit(true)
      this.GeneralService.stopLoading();
    }else{
      const access_user = getQueryStringValue('access_user');
      const uid = getQueryStringValue('uid');
      this.GeneralService.startLoading('درحال دریافت اطلاعات از سرور')
      await this.bundle.callPublicToken();
      this.auth.PUBLIC_TOKEN_OBSERVABLE.emit(true)
      this.GeneralService.startLoading('درحال دریافت اطلاعات حساب کاربری')
      await this.bundle.decideLoginLogic();
      this.GeneralService.stopLoading()
      let refidDecrypted = localStorage.getItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09')) ? await this.auth.SERVER_SIDE_ENCRYPTION(localStorage.getItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09')).toString(), 'decrypt') : null
      refidDecrypted = refidDecrypted ? `${refidDecrypted}` : '';
      if(!this.auth.REF_ID){
        if(refidDecrypted){
          this.auth.REF_ID = `${refidDecrypted}`;
          if(getQueryStringValue('op') == 'tm'){
            this.Router.navigate(['/شارژ'])
          }else if(getQueryStringValue('op') == 'bm'){
            this.Router.navigate(['/اینترنت'])
          }else if(getQueryStringValue('op') == 'vm'){
            this.Router.navigate(['/کارت-شارژ'])
          }else if(getQueryStringValue('ref')){
            const index = window.location.pathname.search('ref')
            if(index){
              this.Router.navigate([getPathFromUrl(window.location.pathname)]);
              setTimeout(() => {
                this.GeneralService.stopLoading()
              }, 1000);
            }
          }
        }else if(getQueryStringValue('ref') && !refidDecrypted){
          const refid = getQueryStringValue('ref');
          try{
            const data = await this.auth.GET_REF_ID_CODE(refid);
            if(data){
              if(data['usercode']){
                this.auth.REF_ID = data['usercode'];
                const ref = await this.auth.SERVER_SIDE_ENCRYPTION('ref', 'encrypt');
                const refId = await this.auth.SERVER_SIDE_ENCRYPTION(refid, 'encrypt');
                localStorage.setItem(`${ref}`, `${refId}`);
                localStorage.setItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09', `${ref}`);
                // if(getQueryStringValue('access_user')){
                //   this.Router.navigate(['ورود'])
                // }
                if(getQueryStringValue('op') == 'tm'){
                  this.Router.navigate(['/شارژ'])
                }else if(getQueryStringValue('op') == 'bm'){
                  this.Router.navigate(['/اینترنت'])
                }else if(getQueryStringValue('op') == 'vm'){
                  this.Router.navigate(['/کارت-شارژ'])
                }else{
                  const index = window.location.pathname.search('ref')
                  if(index){
                    this.Router.navigate([getPathFromUrl(window.location.pathname)])
                    setTimeout(() => {
                      this.GeneralService.stopLoading()
                    }, 1000);
                  }
                }
                this.GeneralService.stopLoading()
              }
            }
          }catch{
            this.GeneralService.stopLoading()
            this.Router.navigate([decodeURIComponent(window.location.pathname)])
          }
          
          
        }else{
          if(localStorage.getItem('refidStatus') !== 'noRefid'){
            this.modal.openModal('refid')
          }
        }
      }else{
        if(refidDecrypted){
          if(getQueryStringValue('op') == 'tm'){
            this.Router.navigate(['/شارژ'])
          }else if(getQueryStringValue('op') == 'bm'){
            this.Router.navigate(['/اینترنت'])
          }else if(getQueryStringValue('op') == 'vm'){
            this.Router.navigate(['/کارت-شارژ'])
          }else{
            this.Router.navigate(['/شارژ'])
          }
        }
      }
     
      if(getQueryStringValue('op') == 'tm'){
        this.Router.navigate(['/شارژ'])
      }else if(getQueryStringValue('op') == 'bm'){
        this.Router.navigate(['/اینترنت'])
      }else if(getQueryStringValue('op') == 'vm'){
        this.Router.navigate(['/کارت-شارژ'])
      }else{
        // if(window.location.pathname == '/'){
        //   this.Router.navigate(['شارژ'])
        // }
      }
      // if(getQueryStringValue('access_user')){
      //   this.auth.GET_PRIVATE_TOKEN(getQueryStringValue('access_user'))
      // }
      
      // this.auth.PUBLIC_TOKEN_OBSERVABLE.subscribe(
      //       if(localStorage.getItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09'))){
      //         const refid = localStorage.getItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09'))
      //         this.auth.GET_REF_ID_CODE(refid)
      //       }else{
      //         if(getQueryStringValue('ref')){
      //           const refid = getQueryStringValue('ref');
      //           localStorage.setItem('ref', refid);
      //           this.auth.GET_REF_ID_CODE(refid)
      //         }
      //       }
      //     }
      //   }
      // )
    }
    
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    sessionStorage.setItem('sid', '')
  }

  onActivate(event) {
    window.scroll(0,0);
  }
  
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.GeneralService.loading = true;
      this.modal.closeModal()
    }
    if (event instanceof NavigationCancel) {
      setTimeout(() => {
        setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
      }, 1000);
    }
    if (event instanceof NavigationError) {
      setTimeout(() => {
        setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
      }, 1000);
    }
   
  }
}
