export interface Response{
    ASREVIRA: Object;
}
export interface AuthenticatedResponse{
    ASREVIRA: {
        access_user: string,
        uid: string,
    };
}