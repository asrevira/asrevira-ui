import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_PREFIX } from '../utils/constant';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  constructor(
    private http: HttpClient,
    private auth: AuthenticationService
  ) { }
  USER_TRANSACTIONS = []
  GET_USER_TRANSACTIONS(page = null, type = null){
    const url = `${API_PREFIX}/${this.auth.$A.PRIVATE}/${this.auth.$B.PRIVATE}/billing/${this.auth.$C.PRIVATE}/@acc_info`;
    const body = {
      access_user: this.auth.ACCESS_USER,
      page,
      type,
    };
    return this.http.post(url, body, {
      headers: this.auth.HEADER.PRIVATE
    })
  }
  GET_TRANSACTION(invoice, key = 'PRIVATE'){
    const url = `${API_PREFIX}/${this.auth.$A[key]}/${this.auth.$B[key]}/billing/search_invoice`;
    const body = {
      access_user: key == 'PRIVATE' ? this.auth.ACCESS_USER : null,
      invoice
    };
    return this.http.post(url, body, {
      headers: this.auth.HEADER[key]
    })
  }
}
