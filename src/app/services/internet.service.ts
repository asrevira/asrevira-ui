import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { API_PREFIX } from '../utils/constant';
import { GeneralService } from './general.service';
import { Router } from '@angular/router';
import { Response } from '../dto/response.model';

@Injectable({
  providedIn: 'root'
})
export class InternetService {

  constructor(
    private http: HttpClient,
    private auth: AuthenticationService,
    private general: GeneralService,
    private router: Router
  ) { }
  internet_packages = null

  SEARCH({op, mobile}){
    const url = `${API_PREFIX}/${this.auth.$A.PUBLIC}/${this.auth.$B.PUBLIC}/internet/bundle_list`;
    const body = {
      op,
      mobile
    };
    return this.http.post(url, body, {
      headers:this.auth.HEADER.PUBLIC
    })
  }

  BUY_INTERNET_PACKAGE({gateway_payment, bundle_id, number, op, simtype, refid, request_id}){
    return new Promise((response, rej) => {
      const url = `${API_PREFIX}/${this.auth.$A.PRIVATE || this.auth.$A.PUBLIC}/${this.auth.$B.PRIVATE || this.auth.$B.PUBLIC}/internet/bundle_buy`;
      const body = {
        access_user: this.auth.ACCESS_USER,
        gateway_payment,
        bundle_id,
        number, 
        op, 
        simtype: 1, 
        request_id,
        refid: this.auth.REF_ID
      };
      this.http.post(url, body, {
        headers: this.auth.HEADER.PRIVATE || this.auth.HEADER.PUBLIC
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
            response()
            return;
          }
          if(gateway_payment == 100){
            this.router.navigate([`/payment/${res.ASREVIRA['invoice']}`])
          }
          else if(res.ASREVIRA['pay_link'] && res.ASREVIRA['status']){
            window.location = res.ASREVIRA['pay_link']
          }
        },
        () => {
          rej()
        }
      )
    })
  }
}
