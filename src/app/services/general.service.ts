import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor(private _snackBar: MatSnackBar) { }
  public loading = true;
  public loadingText = 'در حال پردازش اطلاعات عصر ویرا';
  openSnackBar(message: string, action: string, period: number = 10000) {
    this._snackBar.open(message, action, {
      duration: period,
    }, );
  }
  startLoading(text = 'در حال پردازش اطلاعات عصر ویرا'){
    this.loadingText = text;
    this.loading = true;
  }
  stopLoading(){
    setTimeout(() => {
      this.loadingText = 'در حال پردازش اطلاعات عصر ویرا';
      this.loading = false;
    }, 300);
  }
  
}
