import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_PREFIX } from '../utils/constant';
import { AuthenticationService } from './authentication.service';
import { GeneralService } from './general.service';
import { Router } from '@angular/router';
import { Response } from '../dto/response.model';

@Injectable({
  providedIn: 'root'
})
export class TopupService {
  constructor(
    private http: HttpClient,
    private auth: AuthenticationService,
    private general: GeneralService,
    private router: Router
  ) { }
  public TOPUP_TRANSACTIONS = {}
  BUY_TOPUP({gateway_payment, amount, number, op, simtype, profile, refid}){
    return new Promise((response, rej) => {
      const url = `${API_PREFIX}/${this.auth.$A.PRIVATE || this.auth.$A.PUBLIC}/${this.auth.$B.PRIVATE || this.auth.$B.PUBLIC}/topup/order`;
      const body = {
        access_user: this.auth.ACCESS_USER,
        gateway_payment,
        amount,
        number, 
        op, 
        simtype, 
        profile, 
        refid: this.auth.REF_ID
      };
      this.http.post(url, body, {
        headers: this.auth.HEADER.PRIVATE || this.auth.HEADER.PUBLIC
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
            response()
            return;
          }
          if(gateway_payment == 100){
            this.router.navigate([`/payment/${res.ASREVIRA['invoice']}`])
          }
          else if(res.ASREVIRA['pay_link'] && res.ASREVIRA['status']){
            window.location = res.ASREVIRA['pay_link']
          }
        },
        () => {
          rej()
        }
      )
    })
  }
  TOPUP_TRACKING({ op, invoice }){
    const url = `${API_PREFIX}/${this.auth.$A.PUBLIC}/${this.auth.$B.PUBLIC}/topup/invoice_status`;
    const body = {
      op, 
      invoice,
    };
    return this.http.post(url, body, {
      headers: this.auth.HEADER.PUBLIC
    })
  }
  GET_VOUCHER_LIST(){
    const url = `${API_PREFIX}/${this.auth.$A.PUBLIC}/${this.auth.$B.PUBLIC}/voucher/voucher_list`;
    const body = {};
    return this.http.post(url, body, {
      headers: this.auth.HEADER.PUBLIC
    })
  }
  BUY_VOUCHER({gateway_payment, vtpcode, isid, count}){
    return new Promise((response, rej) => {
      const url = `${API_PREFIX}/${this.auth.$A.PRIVATE || this.auth.$A.PUBLIC}/${this.auth.$B.PRIVATE || this.auth.$B.PUBLIC}/voucher/order`;
      const body = {
        access_user: this.auth.ACCESS_USER || null,
        gateway_payment,
        vtpcode,
        isid, 
        count, 
        refid: this.auth.REF_ID
      };
      this.http.post(url, body, {
        headers: this.auth.HEADER.PRIVATE || this.auth.HEADER.PUBLIC
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
            response()
            return;
          }
          if(gateway_payment == 100){
            this.router.navigate([`/payment/${res.ASREVIRA['invoice']}`])
          }
          else if(res.ASREVIRA['pay_link'] && res.ASREVIRA['status']){
            window.location = res.ASREVIRA['pay_link']
          }
        },
        () => {
          rej()
        }
      )
    })
  }
  GET_TOPUP_TRANSACTIONS(page = 0){
    const url = `${API_PREFIX}/${this.auth.$A.PRIVATE}/${this.auth.$B.PRIVATE}/topup/${this.auth.$C.PRIVATE}/@billing`;
    const body = {
      access_user: this.auth.ACCESS_USER,
      page,
    };
    this.http.post(url, body, {
      headers: this.auth.HEADER.PRIVATE
    }).subscribe(
      (res: Response) => {
        if(res.ASREVIRA['msg']){
          this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
        }
        if(res.ASREVIRA['status']){
          this.TOPUP_TRANSACTIONS = res.ASREVIRA;
        }
      } 
    )
  }
}
