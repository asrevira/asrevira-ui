import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { getQueryStringValue } from '../utils/helper';
import { GeneralService } from './general.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BundleService {

  constructor(private auth: AuthenticationService, private general: GeneralService, private router: Router) { }
  async callPublicToken(){
    const publicToken = await this.auth.GET_PUBLIC_TOKEN();
    await this.auth.PROCESS_TOKEN(publicToken, 'PUBLIC');
  }
  async callPrivateToken(access_user){
    const privateToken = await this.auth.GET_PRIVATE_TOKEN(access_user);
    await this.auth.PROCESS_TOKEN(privateToken, 'PRIVATE');
  }
  
  async getProfile(){
    this.general.startLoading('درحال دریافت اطلاعات پروفایل شما')
    await this.auth.GET_USER_INFO();
    this.general.startLoading('درحال دریافت اطلاعات کیف پول شما')
    await this.auth.GET_USER_EWALLET();
  }
  async normalLogin(obj){
    this.auth.LOGOUT()
    await this.callPublicToken()
    this.general.startLoading()
    const response = await this.auth.LOGIN(obj);
    localStorage.setItem('auth', response['auth']);
    localStorage.setItem('pwd', response['pwd']);
    localStorage.setItem('access_user', response['access_user']);
    localStorage.setItem('uid', response['uid']);
    await this.callPrivateToken(response['access_user'])
    await this.auth.PROCESS_USER_ACCESS(response['uid'])
    await this.getProfile()
    if(window.location.pathname.search('%D9%88%D8%B1%D9%88%D8%AF') > -1){
      this.router.navigate(['/'])
    }
    this.general.stopLoading()
  }
  async accessUserLogin(sid, access_user, uid){
    if(!access_user && !uid){
      if(sid){
        sessionStorage.setItem('sid', sid)
        const response = await this.auth.getAccessUserBySessionId(sid);
        try{
          await this.callPrivateToken(response['access_user'])
          await this.auth.PROCESS_USER_ACCESS(response['uid']);
        }catch{
          this.router.navigate(['ورود'])
        }
        localStorage.setItem('access_user', response['access_user']);
        localStorage.setItem('uid', response['uid']);
        try{
          await this.getProfile();
        }catch{
          this.router.navigate(['ورود'])
        }
        if(window.location.pathname.search('%D9%88%D8%B1%D9%88%D8%AF') > -1){
          this.router.navigate(['/'])
        }
      }
    }else{
      await this.callPrivateToken(access_user)
      await this.auth.PROCESS_USER_ACCESS(uid);
      try{
        await this.getProfile();
      }catch{
        this.router.navigate(['ورود'])
      }
      if(window.location.pathname.search('%D9%88%D8%B1%D9%88%D8%AF') > -1){
        this.router.navigate(['/'])
      }
    }
    
  }
  async decideLoginLogic(){
    // if(getQueryStringValue('access_user')){
    //   this.router.navigate(['ورود'])
    //   return
    // }
    const sid = getQueryStringValue('sid');
    const access_user = localStorage.getItem('access_user') || null;
    const uid = localStorage.getItem('uid') || null;
    if(sid || (access_user && uid)){
      await this.accessUserLogin(sid, access_user, uid)
    }
  }
}
