import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_PREFIX } from '../utils/constant';
import { sha512 } from 'js-sha512';
import { CookieService } from 'ngx-cookie-service';
import { setCookie, deleteCookie, clearFlatObject } from '../utils/helper';
import { Router } from '@angular/router';
import { TransactionsService } from './transactions.service';
import { GeneralService } from './general.service';
import { Response, AuthenticatedResponse } from '../dto/response.model';
import { ModalService } from './modal.service';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private http: HttpClient,
    private Router: Router,
    private general: GeneralService,
    private modal: ModalService
  ) { }
  public HEADER = {
    PUBLIC: null,
    PRIVATE: null
  };
  x = null;
  public TOKEN = {
    PUBLIC: {
      access_user: "",
      key_private: "",
      key_public: "",
      token: "",
    },
    PRIVATE: {
      access_user: "",
      key_private: "",
      key_public: "",
      token: "",
    }
  };
  public ACCESS_URL = {
    PUBLIC: null,
    PRIVATE: null
  };
  public REF_USER = null; 
  public USER_TYPE = 'guest'
  public REF_ID = ''
  public PUBLIC_TOKEN_OBSERVABLE = new EventEmitter<Boolean>();
  public AUTHENTICATED_OBJECT = {};
  public IS_AUTHENTICATED = false;
  public IS_AUTHENTICATED_OBSERVABLE = new EventEmitter<Boolean>();
  public USER_INFO_OBSERVABLE = new EventEmitter<Boolean>();
  public USER_EWALLET_OBSERVABLE = new EventEmitter<Boolean>();
  public USER_INFO = null
  public USER_EWALLET = null
  public ACCESS_USER = null;
  public $A = {
    PUBLIC: '',
    PRIVATE: ''
  };
  public $B = {
    PUBLIC: '',
    PRIVATE: ''
  };
  public $C = {
    PUBLIC: '',
    PRIVATE: ''
  };
  errors = {
    login: new EventEmitter<string>()
  }
  async autoLogin(newAuth, newPwd){
    const auth = await this.SERVER_SIDE_ENCRYPTION(newAuth, 'decrypt')
    const pwd = await this.SERVER_SIDE_ENCRYPTION(newPwd, 'decrypt')
    return await this.LOGIN({auth, pwd})
  }
  GET_PUBLIC_TOKEN(){
    const url = `${API_PREFIX}/@get:token`;
    return new Promise((response, rej) => {
      this.http.get(url).subscribe(
        (res: Response) => {
          delete res.ASREVIRA['request']['agent'];
          response(res.ASREVIRA)
        } 
      )
    })
  }

  GET_PRIVATE_TOKEN(access_user){
    const url = `${API_PREFIX}/@get:token`;
    const body = {
      access_user
    };
    return new Promise((response, rej) => {
      this.http.post(url, body).subscribe(
        (res: Response) => {
          delete res.ASREVIRA['request']['agent'];
          response(res.ASREVIRA)
        } 
      )
    })
  }

  // GET_REF_ID_INFO(refidEncode){
  //   const url = `${API_PREFIX}/@get:token`
  //   const body = {
  //     usercode: refidEncode
  //   };
  //   this.http.post(url, body).subscribe(
  //     (res: Response) => {
  //       delete res.ASREVIRA['request']['agent'];
  //       this.PROCESS_TOKEN(res.ASREVIRA, 'PRIVATE');
  //       this.PROCESS_USER_ACCESS(data)
  //       this.GET_USER_INFO()
  //       this.GET_USER_EWALLET()
  //     } 
  //   )
  // }

  async GET_REF_ID_CODE(refidEncode, notShowError = false){
    const url = `${API_PREFIX}/${this.ACCESS_URL.PUBLIC}@link_get_refid`
    const body = {
      refid: refidEncode,
      type: 'webapp'
    };
    return new Promise((response, rej) => {
      this.http.post(url, body, {
        headers: this.HEADER.PUBLIC
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA){
            if(res.ASREVIRA['info']){
              response(res.ASREVIRA['info'])
            }
          }
          // this.GET_REF_ID_INFO(res.ASREVIRA)
        },
        err => {
          if(!notShowError){
            this.general.openSnackBar('کد معرف معتبر نیست!', 'بستن')
          }
          rej(err);
        }
      )
    })
    
  }
  
  LOGIN({auth, pwd}, autologin = false){
    const url = `${API_PREFIX}/${this.ACCESS_URL.PUBLIC}@login`;
    const body = {
      auth,
      pwd: sha512(pwd),
    }
    return new Promise((response,rej) => {
      this.http.post(url, body, {
        headers: this.HEADER.PUBLIC
      }).subscribe(
        (res: AuthenticatedResponse) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
          }
          delete res.ASREVIRA['request']['agent'];
          
          response(res.ASREVIRA)
        },
        async (err) => {
          if(err.error){
            if(err.error.ASREVIRA){
              if(err.error.ASREVIRA.error_code == 403){
                this.general.openSnackBar('حسابی با این نام کاربری وجود ندارد.', 'بستن')
              }
              if(err.error.ASREVIRA.error_code == 404){
                this.general.openSnackBar('نام کاربری یا رمز عبور اشتباه است.', 'بستن')
              }
            }
          }
          this.LOGOUT()
          const publicToken = await this.GET_PUBLIC_TOKEN();
          await this.PROCESS_TOKEN(publicToken, 'PUBLIC');
          this.general.stopLoading()
          rej(err)
        }
      )
    }) 
  }
  GET_USER_INFO(){
    const url = `${API_PREFIX}/${this.ACCESS_URL.PRIVATE}@profile`;
    const body = {
      access_user: this.ACCESS_USER
    };
    const options = this.HEADER.PRIVATE;
    return new Promise((response, rej) => {
      this.http.post(url, body, {
        headers: this.HEADER.PRIVATE
      }).subscribe(
        async (res: Response) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
          }
          this.USER_INFO = res.ASREVIRA['profile'];
          if(res.ASREVIRA['profile']['reagent']){
            this.REF_ID = res.ASREVIRA['profile']['reagent']['usercode'];
            const refid = await this.SERVER_SIDE_ENCRYPTION(res.ASREVIRA['profile']['reagent']['usercode'], 'encrypt');
            localStorage.setItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09'), `${refid}`)
          }
          this.USER_INFO_OBSERVABLE.emit(this.USER_INFO)
          response(this.USER_INFO)
        },
        async err => {
          if(err.error.ASREVIRA.error_code == 403){
            this.LOGOUT()
            const publicToken = await this.GET_PUBLIC_TOKEN();
            await this.PROCESS_TOKEN(publicToken, 'PUBLIC');
            this.general.loading = false;
            this.Router.navigate(['ورود'])
          }
          rej(err);
        }
      )
    })
  }

  SERVER_SIDE_ENCRYPTION(data, mode = 'encrypt'){
    let {key_public, key_private} = this.TOKEN.PUBLIC;
    if(mode == 'decrypt'){
      const token = localStorage.getItem(data) ? JSON.parse(localStorage.getItem(data)) : null;
      key_public = token ? token.key_public : key_public
      key_private = token ? token.key_private : key_private
    }
    const url = `${API_PREFIX}/@encryption`;
    const body = { data, mode, key_public, key_private }
    return new Promise((response, rej) => {
      this.http.post(url, body).subscribe(
        (res: Response) => {
          if(mode == 'encrypt'){
            localStorage.setItem(res.ASREVIRA['data'], JSON.stringify(res.ASREVIRA))
          }
          response(res.ASREVIRA['data'])
        }
      )
    })
  }
  
  GET_USER_EWALLET(){
    const url = `${API_PREFIX}/${this.ACCESS_URL.PRIVATE}@get_ewallet`;
    const body = {
      access_user: this.ACCESS_USER
    };
    return new Promise((response, rej) => {
      this.http.post(url, body, {
        headers: this.HEADER.PRIVATE
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
          }
          this.USER_EWALLET = res.ASREVIRA['ewallet'];
          this.USER_EWALLET_OBSERVABLE.emit(this.USER_EWALLET);
          response(this.USER_EWALLET)
        }
      )
    }) 
  }

  LOGOUT(){
    // this.x = clearFlatObject(this.x);
    // this.TOKEN = clearFlatObject(this.TOKEN);
    // this.$A = clearFlatObject(this.$A);
    // this.$B = clearFlatObject(this.$B);
    this.AUTHENTICATED_OBJECT = {};
    this.IS_AUTHENTICATED = false;
    this.ACCESS_USER = '';
    this.REF_USER = '';
    this.IS_AUTHENTICATED_OBSERVABLE.emit(this.IS_AUTHENTICATED);
    // this.$C = clearFlatObject(this.$C);
    // this.ACCESS_URL = clearFlatObject(this.ACCESS_URL);
    localStorage.setItem('auth', '')
    localStorage.setItem('pwd', '');
    localStorage.setItem('access_user', '');
    localStorage.setItem('uid', '');
    const path = window.location.pathname;
    if(path.search('%D8%AA%D8%B1%D8%A7%DA%A9%D9%86%D8%B4%E2%80%8C%D9%87%D8%A7') > -1 || path.search('%D9%BE%D8%B1%D9%88%D9%81%D8%A7%DB%8C%D9%84') > -1){
      this.Router.navigate(['ورود'])
    }
    this.GET_PUBLIC_TOKEN()
  }

  getAccessUserBySessionId(sid){
    const url = `${API_PREFIX}/${this.ACCESS_URL.PUBLIC}@get_session`;
    const body = {
      sid,
    };
    return new Promise((response, rej) => {
      this.http.post(url, body, {
        headers: this.HEADER.PUBLIC
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA['msg']){
            this.general.openSnackBar(res.ASREVIRA['msg'], 'بستن')
          }
          response(res.ASREVIRA)
        }
      )
    })
  }

  PROCESS_TOKEN(data, status = 'PUBLIC'){
    return new Promise((response, reject) => {
      if (status == 'PUBLIC') {
        this.TOKEN.PUBLIC = data;
        this.$A.PUBLIC = this.TOKEN.PUBLIC.key_private;
        this.$B.PUBLIC = `v${data['request']['api_version']}`;
        this.HEADER.PUBLIC = new HttpHeaders({
          'Content-Type': 'application/json',
          'key-public': this.TOKEN.PUBLIC.key_public,
          'token': this.TOKEN.PUBLIC.token,
        });
        this.ACCESS_URL[status] = `${this.$A[status]}/${this.$B[status]}/`;
        response(this.TOKEN.PUBLIC)
      } else if (status == 'PRIVATE'){
        this.TOKEN.PRIVATE = data;
        this.$A.PRIVATE = data.key_private;
        this.$B.PRIVATE = `v${data['request']['api_version']}`;
        this.HEADER.PRIVATE = new HttpHeaders({
          'Content-Type': 'application/json',
          'key-public': data.key_public,
          'token': data.token,
        });
        this.ACCESS_USER = data.access_user
        response(data)
      }
    })
  }
  PROCESS_USER_ACCESS(uid){
    return new Promise((response, reject) => {
      this.$C.PRIVATE = `a${uid}`;
      this.ACCESS_URL.PRIVATE = `${this.$A.PRIVATE}/${this.$B.PRIVATE}/user/${this.$C.PRIVATE}/`;
      this.USER_TYPE = 'vip';
      this.IS_AUTHENTICATED = true
      this.IS_AUTHENTICATED_OBSERVABLE.emit(this.IS_AUTHENTICATED);
      response()
    })
  }
  async validateRefid(refid){
    const url = `${API_PREFIX}/${this.ACCESS_URL.PUBLIC}@link_get_refid`;
    const body = {
      refid,
      type: 'wepapp'
    };
    return new Promise((response, rej) => {
      this.http.post(url, body, {
        headers: this.HEADER.PRIVATE
      }).subscribe(
        (res: Response) => {
          if(res.ASREVIRA['uid']){
            response(res)
          }
        },
        err => {
          rej(err['ASREVIRA'])
        }
      )
    })
    
  }
  // GET_USER_REFERID(){
  //   const url = `${API_PREFIX}‫‪${this.ACCESS_URL.PRIVATE}@get_ewallet‬‬`;
  //   const body = {
  //     access_user: this.ACCESS_USER
  //   };
  //   this.http.post(url, body, {
  //     headers: this.HEADER
  //   }).subscribe(
  //     (res: Response) => {
  //       this.USER_EWALLET = res.ASREVIRA;
  //     } 
  //   )
  // }
}
