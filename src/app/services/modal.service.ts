import { Injectable, EventEmitter } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ModalService {
  constructor() { }
  modalNameObservable = new EventEmitter()
  isModalOpen: boolean = false;
  modal = null;
  public data = null
  openModal(modalName?, data = null){
      this.modal = modalName;
      this.data = data || null
    if(this.isModalOpen == true){
      this.closeModal()
      setTimeout(() => {
        this.modalNameObservable.emit(this.modal);
        this.isModalOpen = true;
      }, 0);
    }else{
      this.modalNameObservable.emit(this.modal);
      this.isModalOpen = true;
    }
  }
  closeModal(){
    this.isModalOpen = false;
  }
}
