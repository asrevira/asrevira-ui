import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClipboardModule } from 'ngx-clipboard';
import { DynamicModule } from 'ng-dynamic-component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { TopupComponent } from './products/topup/topup.component';
import { InternetPackagesComponent } from './products/internet-packages/internet-packages.component';
import { InternetPackagesResultsComponent } from './products/internet-packages/internet-packages-results/internet-packages-results.component';
import { ProductsMenuComponent } from './shared/products-menu/products-menu.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { InvoiceModalComponent } from './modal/invoice-modal/invoice-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatButtonModule, MatFormFieldModule, MatSnackBarModule } from '@angular/material';
import { ProductsComponent } from './products/products.component';
import { FormsModule } from '@angular/forms';
import { PersianNumberPipe } from './pipe/persian-number.pipe';
import { RialToTomanPipe } from './pipe/rial-to-toman.pipe';
import { NumberSeperatorPipe } from './pipe/number-seperator.pipe';
import { SigninModalComponent } from './modal/signin-modal/signin-modal.component';
import { FactorModalComponent } from './modal/factor-modal/factor-modal.component';
import { TrackingModalComponent } from './modal/tracking-modal/tracking-modal.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalComponent } from './modal/modal.component';
import { LoginComponent } from './pages/login/login.component';
import { AboutComponent } from './pages/about/about.component';
import { TopupTrackingComponent } from './products/topup/topup-tracking/topup-tracking.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { JalaliPipe } from './pipe/jalali.pipe';
import { ProfileComponent } from './pages/profile/profile.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { AfterPaymentComponent } from './pages/after-payment/after-payment.component';
import { EmptystateComponent } from './shared/emptystate/emptystate.component';
import { RefidModalComponent } from './modal/refid-modal/refid-modal.component';
import { ChargeCodeComponent } from './products/charge-code/charge-code.component';
import { ReferUserModalComponent } from './modal/refer-user-modal/refer-user-modal.component';
import { Ng5SliderModule } from 'ng5-slider';
import { AfterPaymentAndroidComponent } from './pages/after-payment-android/after-payment-android.component';
@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    TopupComponent,
    InternetPackagesComponent,
    InternetPackagesResultsComponent,
    ProductsMenuComponent,
    HeaderComponent,
    FooterComponent,
    InvoiceModalComponent,
    ProductsComponent,
    PersianNumberPipe,
    RialToTomanPipe,
    NumberSeperatorPipe,
    SigninModalComponent,
    FactorModalComponent,
    TrackingModalComponent,
    ModalComponent,
    LoginComponent,
    AboutComponent,
    TopupTrackingComponent,
    TransactionsComponent,
    JalaliPipe,
    ProfileComponent,
    LoadingComponent,
    NavigationComponent,
    AfterPaymentComponent,
    EmptystateComponent,
    RefidModalComponent,
    ChargeCodeComponent,
    ReferUserModalComponent,
    AfterPaymentAndroidComponent
  ],
  imports: [
    Ng5SliderModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ClipboardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    HttpClientModule,
    DynamicModule.withComponents([
      SigninModalComponent,
      FactorModalComponent,
      TrackingModalComponent,
      InvoiceModalComponent,
      RefidModalComponent,
      ReferUserModalComponent
    ]),
  ],
  exports: [
      MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
      MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
