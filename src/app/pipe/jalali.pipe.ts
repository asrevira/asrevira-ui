import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'jalali-moment';
@Pipe({
  name: 'jalali'
})
export class JalaliPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let MomentDate = moment(+value*1000);
    if(args == 'hour'){
      return MomentDate.locale('fa').format('HH:mm');
    }else if(args == 'full'){
      return MomentDate.locale('fa').format('ساعت HH:mm روز D MMMM YYYY');
    }else if(args == 'date'){
      return MomentDate.locale('fa').format('D MMMM YYYY');
    }else{
      return MomentDate.locale('fa').format('D-MMMM');
    }
  }
}
