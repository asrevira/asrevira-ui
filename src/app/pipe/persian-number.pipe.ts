import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'persianNumber'
})
export class PersianNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var id = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    if (value) {
      value = value.toString();
      return value.replace(
        /['0','1','2','3','4','5','6','7','8','9']/g,
        function(w) {
          return id[w];
        }
      );
    } else {
      return "";
    }
  }

}
