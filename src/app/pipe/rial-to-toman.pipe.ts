import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rialToToman'
})
export class RialToTomanPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Math.floor(value/10);
  }
  
}
