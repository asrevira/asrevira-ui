import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { trigger, state, style, transition, animate, group, query, animateChild } from '@angular/animations';
import { ModalService } from '../services/modal.service';
import { SigninModalComponent } from './signin-modal/signin-modal.component';
import { InvoiceModalComponent } from './invoice-modal/invoice-modal.component';
import { TrackingModalComponent } from './tracking-modal/tracking-modal.component';
import { RefidModalComponent } from './refid-modal/refid-modal.component';
import { FactorModalComponent } from './factor-modal/factor-modal.component';
import { ReferUserModalComponent } from './refer-user-modal/refer-user-modal.component';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('modal',[
      state('*',style({
        'opacity': '1'
      })),
      transition('void => *',[style({
        'opacity': '0'
      }),animate(200)]),
      transition('* => void',[animate(200,style({
          'opacity': '0'
        })),
      ]),
      
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit {

  constructor(private modalService: ModalService) { }
  component = this.modalService.modal
  ngOnInit() {
    this.modalService.modalNameObservable.subscribe(
      (modalName)=>{
        switch(modalName){
          case 'signin':
          this.component = SigninModalComponent
          break;
          case 'invoice':
          this.component = InvoiceModalComponent
          break;
          case 'tracking':
          this.component = TrackingModalComponent
          break;
          case 'factor':
          this.component = FactorModalComponent
          break;
          case 'refid':
          this.component = RefidModalComponent
          break;
          case 'refer-user':
          this.component = ReferUserModalComponent
          break;
        }
      }
    )
  }
  closeModal(component){
    if (event.target !== event.currentTarget ) return;
    else{
      this.modalService.closeModal()
    }
  }
  isModalOpen(){
    return this.modalService.isModalOpen
  }
}
