import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactorModalComponent } from './factor-modal.component';

describe('FactorModalComponent', () => {
  let component: FactorModalComponent;
  let fixture: ComponentFixture<FactorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
