import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TopupService } from 'src/app/services/topup.service';
import { InternetService } from 'src/app/services/internet.service';
import { MatSnackBar } from '@angular/material';
import { makeTable, detectOperator } from 'src/app/utils/helper';

@Component({
  selector: 'app-factor-modal',
  templateUrl: './factor-modal.component.html',
  styleUrls: ['./factor-modal.component.scss']
})
export class FactorModalComponent implements OnInit {
  constructor(
    private modalService: ModalService,
    private auth: AuthenticationService,
    private topup: TopupService,
    private internet: InternetService,
    private _snackBar: MatSnackBar
  ) { }
  data = this.modalService.data;
  table = makeTable(this.modalService.data);
  ewallet = null;
  loading = false;
  gateway_payment = 10
  closeModal(){
    this.modalService.closeModal()
  }
  getHeaderTitle(){
    if(this.data.type == 'topup'){
      return `شارژ ${detectOperator(this.data.data.number).description}`;
    }
    if(this.data.type == 'internet'){
      return `بسته اینترنت ${detectOperator(this.data.data.number).description}`;
    }
    return ''
  }
  
  pay(){
    this.loading = true;
    if(this.data.type == 'topup'){
      this.topup.BUY_TOPUP({...this.data.data, gateway_payment: this.gateway_payment}).then(() => {
        // this.loading = false;
      })
    }else if(this.data.type == 'internet'){
      this.internet.BUY_INTERNET_PACKAGE({...this.data.data, gateway_payment: this.gateway_payment}).then(() => {
        // this.loading = false;
      })
    }
  }
  ngOnInit() {
    if(this.auth.USER_EWALLET){
      this.ewallet = this.auth.USER_EWALLET;
    }
  }
}
