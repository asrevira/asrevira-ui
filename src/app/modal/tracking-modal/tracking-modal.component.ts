import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-tracking-modal',
  templateUrl: './tracking-modal.component.html',
  styleUrls: ['./tracking-modal.component.scss']
})
export class TrackingModalComponent implements OnInit {

  constructor(
    private modalService: ModalService
  ) { }
  data = this.modalService.data;
  ngOnInit() {
  }
  

}
