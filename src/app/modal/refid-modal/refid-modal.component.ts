import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-refid-modal',
  templateUrl: './refid-modal.component.html',
  styleUrls: ['./refid-modal.component.scss']
})
export class RefidModalComponent implements OnInit {

  constructor(
    private modal: ModalService,
    private auth: AuthenticationService,
    private general: GeneralService
  ) { }
  refid = ''
  loading = false;
  ngOnInit() {
  }
  async addRefid(refid){
    this.loading = true;
    try{
      const data = await this.auth.GET_REF_ID_CODE(refid);
      this.loading = false;
      if(data){
        this.modal.openModal('refer-user', data)
      }
    }catch{
      this.loading = false;
    }
  }
  closeModal(){
    localStorage.setItem('refidStatus', 'noRefid')
    this.modal.closeModal()
  }
}
