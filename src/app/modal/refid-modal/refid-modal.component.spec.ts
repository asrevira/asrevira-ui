import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefidModalComponent } from './refid-modal.component';

describe('RefidModalComponent', () => {
  let component: RefidModalComponent;
  let fixture: ComponentFixture<RefidModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefidModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefidModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
