import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { detectOperator, makeTable } from 'src/app/utils/helper';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TopupService } from 'src/app/services/topup.service';
import { InternetService } from 'src/app/services/internet.service';
import { MatSnackBar } from '@angular/material';
import { GeneralService } from 'src/app/services/general.service';
@Component({
  selector: 'app-invoice-modal',
  templateUrl: './invoice-modal.component.html',
  styleUrls: ['./invoice-modal.component.scss']
})
export class InvoiceModalComponent implements OnInit {

  constructor(
    private modalService: ModalService,
    public auth: AuthenticationService,
    private topup: TopupService,
    private internet: InternetService,
    private general: GeneralService
  ) { }
  data = this.modalService.data;
  table = makeTable(this.modalService.data);
  ewallet = null;
  loading = false;
  gateway_payment = 10;
  refidLoading = false;
  reagent;
  refid = ''
  closeModal(){
    this.modalService.closeModal()
  }
  getHeaderTitle(){
    if(this.data.type == 'topup'){
      return `شارژ ${detectOperator('', this.data.data.op).description}`;
    }
    if(this.data.type == 'internet'){
      return `بسته اینترنت ${detectOperator('', this.data.data.op).description}`;
    }
    if(this.data.type == 'chargeCode'){
      return `کارت شارژ ${detectOperator(this.data.data.isid).description}`;
    }
    return ''
  }
  
  pay(){
    this.loading = true;
    if(this.data.type == 'topup'){
      this.topup.BUY_TOPUP({...this.data.data, gateway_payment: this.gateway_payment}).then(() => {
        this.loading = false;
      }).catch(err => {
        this.loading = false;
      })
    }else if(this.data.type == 'internet'){
      this.internet.BUY_INTERNET_PACKAGE({...this.data.data, gateway_payment: this.gateway_payment}).then(() => {
        this.loading = false;
      }).catch(err => {
        this.loading = false;
      })
    }else if(this.data.type == 'chargeCode'){
      this.topup.BUY_VOUCHER({...this.data.data, gateway_payment: this.gateway_payment}).then(() => {
        this.loading = false;
      }).catch(err => {
        this.loading = false;
      })
    }
  }
  async submitRefid(refid){
    const data = await this.auth.GET_REF_ID_CODE(refid);
    if(data){
      this.auth.REF_ID = data['usercode']
      this.general.openSnackBar(`${data['fname']} ${data['lname']} به عنوان معرف شما ثبت شد.`, 'بستن')
      localStorage.setItem('ref', data['usercode'])
    }else{
      this.general.openSnackBar('کد معرف وارد شده معتبر نیست.', 'بستن')
    }
  }
  async hasRefid(){
    return this.auth.REF_ID;
  }
  getStatus(code){
    switch(code){
      case 0:
        return 'غیر فعال';
      case 1:
        return 'فعال';
      case 2:
        return 'غیر فعال';
      case 3:
        return 'بازداشت شده';
      case 4:
        return 'بدهی سیستمی';
      case 5:
        return 'غیر فعال سیستمی';
    }
  }
  async ngOnInit() {
    this.general.stopLoading()
    let refidDecrypted = localStorage.getItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09')) ? await this.auth.SERVER_SIDE_ENCRYPTION(localStorage.getItem(localStorage.getItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09')).toString(), 'decrypt') : null
    refidDecrypted = refidDecrypted ? `${refidDecrypted}` : '';
    if(this.auth.USER_EWALLET){
      this.ewallet = this.auth.USER_EWALLET;
    }
    if(refidDecrypted){
      try{
        this.reagent = await this.auth.GET_REF_ID_CODE(refidDecrypted, true)
      }catch{
      }
    }else if(this.auth.USER_INFO){
      this.reagent = this.auth.USER_INFO.reagent;

    }
  }

}
