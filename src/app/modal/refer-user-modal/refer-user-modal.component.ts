import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { GeneralService } from 'src/app/services/general.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-refer-user-modal',
  templateUrl: './refer-user-modal.component.html',
  styleUrls: ['./refer-user-modal.component.scss']
})
export class ReferUserModalComponent implements OnInit {

  constructor(
    private modal: ModalService,
    private general: GeneralService,
    private auth: AuthenticationService
  ) { }
  reagent = this.modal.data;
  loading = false;
  async ngOnInit() {

  }
  submit(){
    this.loading = true;
    setTimeout(async () => {
      this.auth.REF_ID = this.reagent['usercode']
      const ref = await this.auth.SERVER_SIDE_ENCRYPTION('ref', 'encrypt');
      const refId = await this.auth.SERVER_SIDE_ENCRYPTION(this.reagent['usercode'], 'encrypt');
      localStorage.setItem(`${ref}`, `${refId}`);
      localStorage.setItem('V1RJeFYySldSWGxQVjNSaFZWUXdPUT09', `${ref}`);
      this.general.openSnackBar(`${this.reagent['fname']} ${this.reagent['lname']} به عنوان معرف شما ثبت شد.`, 'بستن');
      this.modal.closeModal()
    }, 2000);
  }
  closeModal(){
    this.modal.closeModal()
  }
}
