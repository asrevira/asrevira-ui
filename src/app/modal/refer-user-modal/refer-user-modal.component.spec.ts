import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferUserModalComponent } from './refer-user-modal.component';

describe('ReferUserModalComponent', () => {
  let component: ReferUserModalComponent;
  let fixture: ComponentFixture<ReferUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
