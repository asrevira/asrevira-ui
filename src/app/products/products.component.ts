import { Component, OnInit } from '@angular/core';
import { slideInAnimation } from '../utils/animation';
import { RouterOutlet, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations: [
    slideInAnimation
    // animation triggers go here
  ]
})
export class ProductsComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    // this.router.navigate(['شارژ'])
  }

  onActivate(event) {
    window.scroll(0,0);
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
