import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternetPackagesResultsComponent } from './internet-packages-results.component';

describe('InternetPackagesResultsComponent', () => {
  let component: InternetPackagesResultsComponent;
  let fixture: ComponentFixture<InternetPackagesResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternetPackagesResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternetPackagesResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
