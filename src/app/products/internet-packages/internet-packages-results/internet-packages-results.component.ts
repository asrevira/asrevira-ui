import { Component, OnInit } from '@angular/core';
import { InternetService } from 'src/app/services/internet.service';
import { persianInternetPackage, getPackageTimeId, setCookie, getCookie, detectOperator, getFavoritePhoneNumber, getDiscount, getSimNamePersian, debounce } from 'src/app/utils/helper';
import { ModalService } from 'src/app/services/modal.service';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Response } from 'src/app/dto/response.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { BundleService } from 'src/app/services/bundle.service';
import { Options, LabelType } from 'ng5-slider';
@Component({
  selector: 'app-internet-packages-results',
  templateUrl: './internet-packages-results.component.html',
  styleUrls: ['./internet-packages-results.component.scss']
})
export class InternetPackagesResultsComponent implements OnInit {

  constructor(
    private internet: InternetService,
    private ModalService: ModalService,
    private GeneralService: GeneralService,
    private auth: AuthenticationService,
    private bundle: BundleService
  ) { }
  messages = {
    phoneNumber: '',
  }
  phoneNumber = ''
  simtype = {
    value: '',
    description: ''
  }
  value: number = 100;
  highValue: number = 400;
  options: Options = {
    floor: 0,
    ceil: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + 'تومان';
        case LabelType.High:
          return value + 'تومان';
        default:
          return value + 'تومان';
      }
    }
  };
  packageType = ''
  filter = ''
  loading = false;
  period = ''
  active = false;
  finalPhoneNumber = ''
  background = {
    simtype: false,
    packagePeriod: false,
    phoneNumber: false
  }
  operator = ''
  requestId = ''
  simtypesList = {
    Mci: [
      {description: 'دائمی', value: '0'},
      {description: 'اعتباری', value: '1'},
      {description: 'دائمی و اعتباری', value: '2'},
      {description: 'همه سیم‌کارت‌ها', value: 'all'},
    ],
    Rightel: [
      {description: 'اعتباری', value: '1'},
      {description: 'دائمی', value: '2'},
      {description: 'همه سیم‌کارت‌ها', value: 'all'},
    ],
    Mtn: [
        {description: 'اعتباری', value: '1'},
        {description: 'دائمی', value: '2'},
        {description: 'TD_LTE اعتباری', value: '3'},
        {description: 'TD_LTE + FDD اعتباری', value: '4'},
        {description: 'اینترنت ثابت  ایرانسل', value: '5'},
        {description: 'مناسبتی', value: '6'},
        {description: 'TD_LTE دائمی', value: '7'},
        {description: 'TD_LTE + FDD دائمی', value: '8'},
        {description: 'همه سیم‌کارت‌ها', value: 'all'},
    ]
  }
  settimeout;
  internet_packages = []
  async ngOnInit() {
    
    this.phoneNumber = localStorage.getItem('phoneNumber');
    this.finalPhoneNumber = localStorage.getItem('phoneNumber');
    this.operator = localStorage.getItem('internet-op') || this.getOperator(this.finalPhoneNumber);
    await this.bundle.callPublicToken()
    this.search()
    setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);

  }
  getFavoriteNumbers(){
    return getFavoritePhoneNumber()
  }
  selectNumber(number){
    this.phoneNumber = number;
    this.operator = this.getOperator(this.phoneNumber);
    this.search()
  }
  getPackages(data){
    const bundles = []
    for(let item in data){
      const obj = { 
        persianDescription: persianInternetPackage(item),
        name: item,
        time: getPackageTimeId(item),
        duration: persianInternetPackage(item),
        packages: [
          ...data[item]
        ]
      }
      bundles.push(obj)
    }
    return bundles;
  }
  getSimNamePersian(type){
    return getSimNamePersian(type)
  }
  persianInternetPackage(){
    return persianInternetPackage(this.period)
  }
  toggleBackground(type){
    this.background[type] = !this.background[type]
  }
  getOperator(phoneNumber){
    return detectOperator(phoneNumber).code
  }
  search(){
    if(!this.phoneNumber.length){
      this.messages.phoneNumber = 'لطفا شماره تلفن خود را وارد کنید.'
    }
    else if(this.phoneNumber.length < 11){
      this.messages.phoneNumber = 'شماره تلفن وارد شده کمتر از ۱۱ رقم است.'
    }
    else if(!detectOperator(this.phoneNumber).code){
      this.messages.phoneNumber = 'اپراتور تشخیص داده نشد.'
    }
    else{
      this.messages.phoneNumber = ''
      this.loading = true;
      this.GeneralService.loading = true;
      this.internet.SEARCH({op: this.operator || detectOperator(this.finalPhoneNumber).code, mobile: this.finalPhoneNumber}).subscribe(
        (res: Response) => {
          this.finalPhoneNumber = this.phoneNumber;
          this.internet.internet_packages = res.ASREVIRA['bundle_list']['bundles'];
          this.requestId = res.ASREVIRA['bundle_list']['requestId'];
          localStorage.setItem('internet_packages', JSON.stringify(this.internet.internet_packages))
          localStorage.setItem('phoneNumber', this.phoneNumber)
          localStorage.setItem('internet-op', this.operator)
          if(!this.internet.internet_packages){
            this.internet.internet_packages = JSON.parse(localStorage.getItem('internet_packages'));
          }
          this.phoneNumber = localStorage.getItem('phoneNumber')
          this.finalPhoneNumber = localStorage.getItem('phoneNumber')
          this.internet_packages = this.getPackages(this.internet.internet_packages);
          // this.findLowestPrice()
          setTimeout(() => {
            this.loading = false;
            this.GeneralService.loading = false;
          }, 1000);
        }
      )
    }
  }
  onChange(phoneNumber){
    this.operator = this.getOperator(phoneNumber)
    clearTimeout(this.settimeout)
    if(this.phoneNumber.length == 11)
    this.settimeout = setTimeout(() => {
      this.search()
    }, 600);
  }
  findLowestPrice(){
    // let packages: any[] = [...this.filterPackage()].map(p => [...p.packages])
    // packages = [].concat.apply([], packages).map(p => Math.floor(p.billAmount/10));
    // this.options.floor == Math.min(...packages)
    // this.options.ceil == Math.max(...packages)
  }
  getInternetBundles(){
    let internetPackages = [...this.internet_packages]
    internetPackages = this.getFilteredPackagesByPeriod(this.period, internetPackages)
    // internetPackages = this.getFilteredPackagesByPeriod(this.period, internetPackages)
    return internetPackages;
  }
  getFilteredPackagesBySimtype(period, bundles){
    return bundles.filter(bundle => bundle.name == period)
  }
  getFilteredPackagesByPeriod(period, bundles){
    return bundles.filter(bundle => bundle.name == period)
  }
  filterPeriod(period){
    this.period = period;
  }
  filterSimType(type){
    this.simtype = type;
  }
  getSimtypes(){
    return this.operator ? this.simtypesList[this.operator] : []
  }
  getSimType(localType){
    if(!localType){
      const type = this.internet_packages[0]['packages'][0].type;
      const simtype = this.getSimtypes().find(_simtype => _simtype.value == type).description;
      return simtype
    }
    const simtype = this.getSimtypes().find(_simtype => _simtype.value == localType) ? this.getSimtypes().find(_simtype => _simtype.value == localType).description : '';
    return simtype
  }
  filterPackage(){
    let filteredData = [...this.internet_packages].filter(pack => pack.name == this.period || !this.period || this.period == 'all');
    filteredData = filteredData.map(pack => {
      const fullpack = {
        ...pack,
        packages: pack.packages.filter(p => p.type == this.simtype.value || !this.simtype.value || this.simtype.value == 'all')
      }
      return fullpack
    })
    return filteredData
  }
  buyPackage(pack, bundle){
    const transaction = {
      type: 'internet',
      package_name: pack.title,
      price: pack.amount,
      finalPrice: pack.billAmount,
      discount: getDiscount(11, this.operator, pack.amount),
      data: {
        gateway_payment: 1,
        bundle_id: pack.id,
        number: this.phoneNumber,
        op: this.operator,
        simtype: pack.type || 'all',
        request_id: this.operator == 'Rightel' ? this.requestId : null,
      }
    }
    if(!this.phoneNumber.length){
      this.messages.phoneNumber = 'لطفا شماره تلفن خود را وارد کنید.'
    }
    else if(this.phoneNumber.length < 11){
      this.messages.phoneNumber = 'شماره تلفن وارد شده کمتر از ۱۱ رقم است.'
    }
    else if(!detectOperator(this.phoneNumber).code){
      this.messages.phoneNumber = 'اپراتور تشخیص داده نشد.'
    }
    else{
      this.messages.phoneNumber = ''
      this.ModalService.openModal('invoice', transaction)
    }
  }

  showEmptyState() {
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    return this.filterPackage().length ? this.filterPackage().map(el => el ? el.packages.length : 0).reduce(reducer) == 0 : true
  }

}
