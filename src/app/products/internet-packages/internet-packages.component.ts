import { Component, OnInit } from '@angular/core';
import { detectOperator, isEmptyObject, setCookie, persianNumberToEnglish, getFavoritePhoneNumber, addToFavoritePhoneNumber } from 'src/app/utils/helper';
import { InternetService } from 'src/app/services/internet.service';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';
import { Response } from 'src/app/dto/response.model';

@Component({
  selector: 'app-internet-packages',
  templateUrl: './internet-packages.component.html',
  styleUrls: ['./internet-packages.component.scss']
})
export class InternetPackagesComponent implements OnInit {

  constructor(
    private internet: InternetService,
    private Router: Router,
    private GeneralService: GeneralService

  ) { }
  
  background = false;
  loading = false;
  ngOnInit() {
    setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
  }
  selectNumber(number){
    this.phoneNumber = number;
    this.updateOperator()
  }
  messages = {
    phoneNumber: '',
    general: '',
  }
  phoneNumber = '';
  operator = ''
  operatorList = [
    {value: 'Mtn', description: 'ایرانسل'},
    {value: 'Mci', description: 'همراه‌اول'},
    {value: 'Rightel', description: 'رایتل'},
  ]
  search(){
    this.phoneNumber = persianNumberToEnglish(this.phoneNumber)
    if(!this.phoneNumber.length){
      this.messages.phoneNumber = 'لطفا شماره تلفن خود را وارد کنید.'
    }
    else if(this.phoneNumber.length < 11){
      this.messages.phoneNumber = 'شماره تلفن وارد شده کمتر از ۱۱ رقم است.'
    }
    else if(!detectOperator(this.phoneNumber).code){
      this.messages.phoneNumber = 'اپراتور تشخیص داده نشد.'
    }
    else{
      this.messages.general = '';
      this.messages.phoneNumber = '';
      this.loading = true
      this.internet.SEARCH({op: this.operator, mobile: this.phoneNumber}).subscribe(
        (res: Response) => {
          addToFavoritePhoneNumber(this.phoneNumber)
          this.internet.internet_packages = res.ASREVIRA['bundle_list']['bundles'];
          localStorage.setItem('internet_packages', JSON.stringify(this.internet.internet_packages))
          localStorage.setItem('phoneNumber', this.phoneNumber)
          this.loading = false
          this.Router.navigate(['اینترنت/بسته‌ها'])
        }
      )
    }
  }
  selectOp(op){
    this.operator = op;
    localStorage.setItem('internet-op', op)
  }
  updateOperator(){
    localStorage.setItem('internet-op', detectOperator(this.phoneNumber).code)
    this.operator = detectOperator(this.phoneNumber).code;
  }
  toggleBackground(){
    this.background = !this.background
  }
  getFavoriteNumbers(){
    return getFavoritePhoneNumber()
  }
  getOperator(phoneNumber){
    return detectOperator(phoneNumber).code;
  }
}
