import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternetPackagesComponent } from './internet-packages.component';

describe('InternetPackagesComponent', () => {
  let component: InternetPackagesComponent;
  let fixture: ComponentFixture<InternetPackagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternetPackagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternetPackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
