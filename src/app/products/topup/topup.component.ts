import { Component, OnInit } from '@angular/core';
import { detectOperator, isEmptyObject, persianNumberToEnglish, getFavoritePhoneNumber, addToFavoritePhoneNumber, getDiscount } from 'src/app/utils/helper';
import { ModalService } from 'src/app/services/modal.service';
import { TopupService } from 'src/app/services/topup.service';
import { GeneralService } from 'src/app/services/general.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { transition } from '@angular/animations';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-topup',
  templateUrl: './topup.component.html',
  styleUrls: ['./topup.component.scss']
})
export class TopupComponent implements OnInit {

  constructor(
    private ModalService: ModalService,
    private topup: TopupService,
    private GeneralService: GeneralService,
    private auth: AuthenticationService
  ) { }
  options: Options = {
    floor: 1000,
    ceil: 99900,
    step: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return `${value} تومان`;
        case LabelType.High:
          return `${value} تومان`;
        default:
          return `${value} تومان`;
      }
    }
  };
  amount: number = 1000;
  phoneNumber = '';
  background = false;
  simtype = 0;
  chargeType = 'direct';
  operator = ''
  paybill = null;
  loading = false;
  operatorList = [
    {value: 'Mtn', description: 'ایرانسل'},
    {value: 'Mci', description: 'همراه‌اول'},
    {value: 'Rightel', description: 'رایتل'},
  ]
  simtypeOptions = [
    {value: 1 ,description: 'پرداخت صورت حساب'},
    {value: 0 ,description: 'افزایش موجودی'}
  ];
  simtypeList = [
    {value: 1 ,description: 'سیم‌کارت دائمی'},
    {value: 0 ,description: 'سیم‌کارت اعتباری'}
  ]
  messages = {
    general: '',
    phoneNumber: '',
  }
  irancellList = [
    {value: 'direct',description: 'معمولی'},
    {value: 'extra',description: 'شگفت‌انگیز'}
  ];
  mciList = [
      {value: 'direct',description: 'معمولی'},
      {value: 'youth',description: 'جوانان'},
      {value: 'lady',description: 'بانوان'},
      {value: 'desire',description: 'فوق‌العاده'},
  ];
  rightelList = [
      {value: 'direct',description: 'معمولی'},
      {value: 'extra',description: 'شورانگیز'}
  ];
  priceList = [
    [
      1000,
      2000,
      5000,
      10000,

    ],
    [
      20000,
      50000,
      99900,
    ],
  ]
  selectItem(price){
    this.amount = price;
  }
  getChargeTypeList(){
    const op = this.operator
    if(op == 'Mci'){
      return this.mciList
    }
    if(op == 'Mtn'){
        return this.irancellList
    }
    if(op == 'Rightel'){
        return this.rightelList
    }
    return this.irancellList
  }
  getSimtypeList(){
    if(this.operator == 'Mtn'){
      return this.simtypeList;
    }
    this.paybill = null;
    this.simtype = 0;
    return [{value: 0 ,description: 'سیم‌کارت اعتباری'}]
  }

  selectNumber(phoneNumber){
    this.phoneNumber = phoneNumber;
    this.updateOperator()
  }

  openModal(modal){
    this.phoneNumber = persianNumberToEnglish(this.phoneNumber)
    const transaction = {
      type: 'topup',
      function: this.topup.BUY_TOPUP,
      discount: getDiscount(12, this.getOperator(this.phoneNumber), this.amount * 10),
      data: {
        gateway_payment: 10,
        amount: this.amount * 10,
        number: this.phoneNumber,
        op: this.operator,
        simtype: this.simtype,
        profile: this.chargeType,
        paybill: this.paybill,
      }
    }
    if(!this.phoneNumber){
      this.messages.phoneNumber = 'لطفا شماره تلفن خود را وارد کنید.'
    }
    else if(this.phoneNumber.length < 11){
      this.messages.phoneNumber = 'شماره تلفن وارد شده کمتر از ۱۱ رقم است.'
    }
    else if(!detectOperator(this.phoneNumber).code){
      this.messages.phoneNumber = 'اپراتور تشخیص داده نشد.'
    }
    else if(this.amount > 99900 || this.amount < 1000){
      this.messages.general = 'مبلغ وارد شده بیشتر از ۹۹٫۹۰۰ تومان یا کمتر از ۱۰۰۰ تومان است.'
    }
    else if(isEmptyObject(transaction.data)){
      this.messages.phoneNumber = ''
      this.messages.general = 'لطفا تمامی فیلدها را پر کنید.'
    }
    else{
      this.loading = true;
      setTimeout(() => {
        addToFavoritePhoneNumber(this.phoneNumber)
        this.messages.general = ''
        this.messages.phoneNumber = '';
        this.loading = false;
        this.ModalService.openModal(modal, transaction)
      }, 1000);
    }
  }
  selectOp(op){
    this.operator = op;
  }
  updateOperator(){
    this.operator = this.getOperator(this.phoneNumber);
  }
  getOperator(phoneNumber){
    return detectOperator(phoneNumber).code
  }
  toggleBackground(){
    this.background = !this.background
  }
  getFavoriteNumbers(){
    return getFavoritePhoneNumber()
  }
  ngOnInit() {
    setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
  }

}
