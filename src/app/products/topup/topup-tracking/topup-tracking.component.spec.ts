import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopupTrackingComponent } from './topup-tracking.component';

describe('TopupTrackingComponent', () => {
  let component: TopupTrackingComponent;
  let fixture: ComponentFixture<TopupTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopupTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopupTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
