import { Component, OnInit } from '@angular/core';
import { TopupService } from 'src/app/services/topup.service';
import { clearFlatObject } from 'src/app/utils/helper';
import { ModalService } from 'src/app/services/modal.service';
import { GeneralService } from 'src/app/services/general.service';
import { Response } from 'src/app/dto/response.model';

@Component({
  selector: 'app-topup-tracking',
  templateUrl: './topup-tracking.component.html',
  styleUrls: ['./topup-tracking.component.scss']
})
export class TopupTrackingComponent implements OnInit {

  constructor(
    private topup: TopupService,
    private modal: ModalService,
    private GeneralService: GeneralService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.GeneralService.loading = false;
    }, 1000);
  }
  loading = false;
  invoiceCode = ''
  messages = {
    invoiceCode: '',
    general: ''
  }
  operator = 'Mtn'
  operatorList = [
    {value: 'Mtn', description: 'ایرانسل'},
    {value: 'Mci', description: 'همراه‌اول'},
    {value: 'Rightel', description: 'رایتل'},
  ]
  tracking(){
    if(!this.invoiceCode){
      this.messages.invoiceCode = 'لطفا کد سیستمی عصر ویرا را وارد کنید.'
    }else{
      this.loading = true;
      this.messages = clearFlatObject(this.messages)
      this.topup.TOPUP_TRACKING({op: this.operator, invoice: this.invoiceCode}).subscribe(
        (res: Response) => {
          this.loading = false;
          this.modal.openModal('tracking', res.ASREVIRA)
        },
        err => {
          this.loading = false;
          this.messages.general = 'کد وارد شده اشتباه است.'
        }
      )
    }
  }
  preventKeydown(event){
    if ((event.which < 48 || event.which > 57) && event.which != 9 && event.which != 13 && event.which != 189 && event.which != 16 && event.which != 8 && event.which != 17 && event.which != 18) {
      event.preventDefault();
    }
  }
}
