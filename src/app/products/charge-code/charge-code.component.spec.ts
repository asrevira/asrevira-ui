import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeCodeComponent } from './charge-code.component';

describe('ChargeCodeComponent', () => {
  let component: ChargeCodeComponent;
  let fixture: ComponentFixture<ChargeCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
