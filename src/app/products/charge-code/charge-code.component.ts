import { Component, OnInit } from '@angular/core';
import { detectOperator, getFavoritePhoneNumber, isEmptyObject, addToFavoritePhoneNumber, persianNumberToEnglish, getDiscount } from 'src/app/utils/helper';
import { ModalService } from 'src/app/services/modal.service';
import { TopupService } from 'src/app/services/topup.service';
import { GeneralService } from 'src/app/services/general.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { InternetService } from 'src/app/services/internet.service';
import { Router } from '@angular/router';
import { Response } from 'src/app/dto/response.model';

@Component({
  selector: 'app-charge-code',
  templateUrl: './charge-code.component.html',
  styleUrls: ['./charge-code.component.scss']
})
export class ChargeCodeComponent implements OnInit {
  constructor(
    private internet: InternetService,
    private Router: Router,
    private GeneralService: GeneralService,
    private topup: TopupService,
    private auth: AuthenticationService,
    private ModalService: ModalService

  ) { }
  background = false;
  loading = false;
  vouchers = [];
  messages = {
    phoneNumber: '',
    general: '',
  }
  count = 1;
  phoneNumber = '';
  operator = 'Mtn'
  operatorList = [
    {value: 'Mtn', description: 'ایرانسل', code: '00000053'},
    {value: 'Mci', description: 'همراه‌اول', code: '00000061'},
    {value: 'Rightel', description: 'رایتل', code: '8921992'},
  ]
  prices = {
    Mtn: [],
    Mci: [],
    Rightel: [],
  }
  amount = {}
  ngOnInit() {
    if(this.auth.ACCESS_URL.PUBLIC){
      this.topup.GET_VOUCHER_LIST().subscribe((res: Response) => {
        this.vouchers = res.ASREVIRA['voucher_list'];
        this.listVouchers(this.vouchers);
        this.GeneralService.loading = false;
      })
    }
    this.auth.PUBLIC_TOKEN_OBSERVABLE.subscribe(
      (res: Response) => {
        if(res){
          this.topup.GET_VOUCHER_LIST().subscribe((res: Response) => {
            this.vouchers = res.ASREVIRA['voucher_list'];
            this.listVouchers(this.vouchers);
            this.GeneralService.loading = false;
          })
        }
      }
    )
    
  }

  preventType(e){
    if(this.count > 1000){
      this.count = 1000;
      e.preventDefault();
    }
  }


  selectItem(item){
    this.amount = item;
  }

  listVouchers(vouchers){
    this.prices.Mtn = vouchers.filter(voucher => voucher.isid == '00000053' && voucher.price !== '0');
    this.prices.Mci = vouchers.filter(voucher => voucher.isid == '00000061' && voucher.price !== '0');
    this.prices.Rightel = vouchers.filter(voucher => voucher.isid == '8921992' && voucher.price !== '0');
    this.makeColumns()
  }
  makeColumns(){
    for (const key in this.prices) {
      if (this.prices.hasOwnProperty(key)) {
        const priceList = []
        const columns = Math.ceil(this.prices[key].length/3)
        this.prices[key].sort((a, b) => a.price/1 - b.price/1);
        const tempPricesKey = [];
        this.prices[key].forEach(item => {
          if(tempPricesKey.findIndex(p => p.price && p.price == item.price) == -1){
            tempPricesKey.push(item)
          }
        })
        for (let index = 1; index <= columns; index++) {
          const arr = [...tempPricesKey.filter((price, i) => {
            let isReplica = false;
            if(i < index*3 && i >= (index - 1)*3 && !isReplica){
              return true
            }
          })]
          priceList.push(arr);
        }
        this.prices[key] = priceList;
      }
    }
    
  }

  handleBlur(e){
    if(+e.target.value <= 0 && e.target.value.toString().length <= 1){
      e.target.value = 1;
      this.count = 1
    }
    if(+e.target.value >= 1000){
      e.target.value = 1000;
      this.count = 1000
    }
  }

  butChargeCode(){
    if(!this.amount['price']){
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        this.GeneralService.openSnackBar('لطفا مبلغ را انتخاب کنید.', 'بستن', 5000)
      }, 1000);
      
    }else if(!this.operator){
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        this.GeneralService.openSnackBar('لطفا اپراتور را انتخاب کنید.', 'بستن', 5000)
      }, 1000);
    }else if(!this.count){
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        this.GeneralService.openSnackBar('لطفا تعداد کارت شارژ ها را انتخاب کنید.', 'بستن', 5000)
      }, 1000);
    }
    else{
      const transaction = {
        type: 'chargeCode',
        function: this.topup.BUY_TOPUP,
        amount: this.amount['price'],
        data: {
          gateway_payment: 10,
          isid: this.amount['isid'],
          vtpcode: this.amount['vtpcode'],
          count: this.count || 1,
        }
      }
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        this.ModalService.openModal('invoice', transaction)
      }, 1000);
    }
  }
  selectNumber(number){
    this.phoneNumber = number;
  }
  toggleBackground(){
    this.background = !this.background
  }
  getFavoriteNumbers(){
    return getFavoritePhoneNumber()
  }
  getOperator(phoneNumber){
    return detectOperator(phoneNumber).code;
  }
}
