import { trigger, transition, style, query, animateChild, group, animate, state } from '@angular/animations';

export const slideInAnimation =
    trigger('routeAnimations', [
        transition('* => *', [
        query(
            ':enter , :leave',
            [style({ opacity: 0 })],
            { optional: true }
        ),
        query(
            ':leave',
            [style({ opacity: 1 }), animate('0.3s', style({ opacity: 0 }))],
            { optional: true }
        ),
        query(
            ':enter',
            [style({ opacity: 0 }), animate('0.3s', style({ opacity: 1 }))],
            { optional: true }
        )
        ])
    ]);
export const loading = trigger('loading',[
    state('*',style({
      'opacity': '1'
    })),
    transition('void => *',[style({
      'opacity': '0'
    }),animate(200)]),
    transition('* => void',[animate(200,style({
        'opacity': '0'
      }))
    ])
  ])