const hamrahaval = `
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5.38326 17.4424C5.36861 17.9672 5.50258 18.2306 5.63603 18.364C5.76947 18.4974 6.03284 18.6314 6.55766 18.6168C7.08538 18.602 7.77936 18.4352 8.60607 18.0809C10.254 17.3746 12.2356 16.007 14.1213 14.1214C16.007 12.2357 17.3746 10.254 18.0808 8.60612C18.4351 7.77941 18.602 7.08543 18.6167 6.55771C18.6314 6.03289 18.4974 5.76952 18.3639 5.63607C18.2305 5.50263 17.9671 5.36866 17.4423 5.38331C16.9146 5.39803 16.2206 5.56488 15.3939 5.91918C13.746 6.62543 11.7644 7.99302 9.87867 9.87871C7.99297 11.7644 6.62538 13.746 5.91913 15.394C5.56483 16.2207 5.39798 16.9146 5.38326 17.4424ZM4.08084 14.6061C4.91013 12.6711 6.4449 10.4841 8.46445 8.4645C10.484 6.44495 12.6711 4.91017 14.6061 4.08089C15.5708 3.66743 16.5218 3.40821 17.3865 3.38408C18.2541 3.35987 19.1306 3.57426 19.7782 4.22186C20.4258 4.86947 20.6401 5.7459 20.6159 6.61349C20.5918 7.47818 20.3326 8.4292 19.9191 9.39395C19.0898 11.329 17.5551 13.516 15.5355 15.5356C13.516 17.5551 11.3289 19.0899 9.39391 19.9192C8.42915 20.3326 7.47813 20.5919 6.61344 20.616C5.74585 20.6402 4.86942 20.4258 4.22181 19.7782C3.57421 19.1306 3.35983 18.2542 3.38404 17.3866C3.40817 16.5219 3.66738 15.5709 4.08084 14.6061Z" fill="#FFB31A"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M3.47296 11.1696C3.0915 11.5303 3 11.8113 3 12C3 12.1887 3.0915 12.4697 3.47296 12.8304C3.85653 13.1932 4.46523 13.5659 5.30032 13.9C6.96497 14.5658 9.33323 15 12 15C14.6668 15 17.035 14.5658 18.6997 13.9C19.5348 13.5659 20.1435 13.1932 20.527 12.8304C20.9085 12.4697 21 12.1887 21 12C21 11.8113 20.9085 11.5303 20.527 11.1696C20.1435 10.8068 19.5348 10.4341 18.6997 10.1C17.035 9.43419 14.6668 9 12 9C9.33323 9 6.96497 9.43419 5.30032 10.1C4.46523 10.4341 3.85653 10.8068 3.47296 11.1696ZM4.55754 8.2431C6.51218 7.46124 9.14392 7 12 7C14.8561 7 17.4878 7.46124 19.4425 8.2431C20.417 8.63292 21.2728 9.1221 21.9013 9.71646C22.5319 10.3128 23 11.0841 23 12C23 12.9159 22.5319 13.6872 21.9013 14.2835C21.2728 14.8779 20.417 15.3671 19.4425 15.7569C17.4878 16.5388 14.8561 17 12 17C9.14392 17 6.51218 16.5388 4.55754 15.7569C3.58299 15.3671 2.72723 14.8779 2.09874 14.2835C1.46814 13.6872 1 12.9159 1 12C1 11.0841 1.46814 10.3128 2.09874 9.71646C2.72723 9.1221 3.58299 8.63292 4.55754 8.2431Z" fill="#6CC7D9"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M8.60593 18.0811C10.2538 17.3748 12.2355 16.0072 14.1212 14.1215C16.0069 12.2358 17.3745 10.2542 18.0807 8.6063C18.435 7.7796 18.6019 7.08562 18.6166 6.55789C18.6312 6.03307 18.4973 5.76971 18.3638 5.63626L19.778 4.22205C20.4256 4.86965 20.64 5.74609 20.6158 6.61368C20.5917 7.47836 20.3325 8.42939 19.919 9.39414C19.0897 11.3291 17.5549 13.5162 15.5354 15.5358C13.5158 17.5553 11.3288 19.0901 9.39377 19.9194C8.42902 20.3328 7.478 20.592 6.61331 20.6162C5.74572 20.6404 4.86928 20.426 4.22168 19.7784L5.63589 18.3642C5.76934 18.4976 6.03271 18.6316 6.55752 18.617C7.08525 18.6022 7.77923 18.4354 8.60593 18.0811Z" fill="#FFB31A"/>
</svg>
`
const irancell = `
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M7.84522 10.5185C6.77441 11.0624 6.46343 11.6404 6.46343 12C6.46343 12.3597 6.77441 12.9377 7.84522 13.4816C8.86083 13.9974 10.3285 14.3435 11.9999 14.3435C13.6714 14.3435 15.1391 13.9974 16.1547 13.4816C17.2255 12.9377 17.5365 12.3597 17.5365 12C17.5365 11.6404 17.2255 11.0624 16.1547 10.5185C15.1391 10.0027 13.6714 9.65654 11.9999 9.65654C10.3285 9.65654 8.86083 10.0027 7.84522 10.5185ZM7.01988 8.84166C8.34207 8.17007 10.1041 7.78174 11.9999 7.78174C13.8958 7.78174 15.6578 8.17007 16.98 8.84166C18.247 9.48521 19.382 10.5477 19.382 12C19.382 13.4524 18.247 14.5149 16.98 15.1584C15.6578 15.83 13.8958 16.2183 11.9999 16.2183C10.1041 16.2183 8.34207 15.83 7.01988 15.1584C5.75289 14.5149 4.61792 13.4524 4.61792 12C4.61792 10.5477 5.75289 9.48521 7.01988 8.84166Z" fill="#006699"/>
<path d="M12.0045 22C10.9048 22 9.80505 21.9455 8.71438 21.8273C5.2788 21.4729 2.52488 18.7193 2.17042 15.2841C1.94319 13.103 1.94319 10.8947 2.17042 8.71362C2.52488 5.27843 5.2788 2.52482 8.71438 2.1704C10.8957 1.9432 13.1043 1.9432 15.2856 2.1704C18.7212 2.52482 21.4751 5.27843 21.8296 8.71362C22.0568 10.8947 22.0568 13.103 21.8296 15.2841C21.4751 18.7193 18.7212 21.4729 15.2856 21.8273C14.204 21.9455 13.1043 22 12.0045 22ZM12.0045 3.82438C10.9684 3.82438 9.93229 3.8789 8.90525 3.97887C6.27857 4.25151 4.25176 6.27809 3.9791 8.90446C3.77005 10.9583 3.77005 13.0485 3.9791 15.1024C4.25176 17.7287 6.26948 19.7553 8.90525 20.0279C10.9593 20.237 13.0498 20.237 15.1038 20.0279C17.7305 19.7553 19.7573 17.7378 20.03 15.1024C20.239 13.0485 20.239 10.9583 20.03 8.90446C19.7573 6.27809 17.7396 4.25151 15.1038 3.97887C14.0768 3.8789 13.0407 3.82438 12.0045 3.82438Z" fill="#FFBE00"/>
</svg>
`
const rightel = `
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.0045 22C10.9048 22 9.80505 21.9455 8.71438 21.8273C5.2788 21.4729 2.52488 18.7193 2.17042 15.2841C1.94319 13.103 1.94319 10.8947 2.17042 8.71362C2.52488 5.27843 5.2788 2.52482 8.71438 2.1704C10.8957 1.9432 13.1043 1.9432 15.2856 2.1704C18.7212 2.52482 21.4751 5.27843 21.8296 8.71362C22.0568 10.8947 22.0568 13.103 21.8296 15.2841C21.4751 18.7193 18.7212 21.4729 15.2856 21.8273C14.204 21.9455 13.1043 22 12.0045 22ZM12.0045 3.82438C10.9684 3.82438 9.93229 3.8789 8.90525 3.97887C6.27857 4.25151 4.25176 6.27809 3.9791 8.90446C3.77005 10.9583 3.77005 13.0485 3.9791 15.1024C4.25176 17.7287 6.26948 19.7553 8.90525 20.0279C10.9593 20.237 13.0498 20.237 15.1038 20.0279C17.7305 19.7553 19.7573 17.7378 20.03 15.1024C20.239 13.0485 20.239 10.9583 20.03 8.90446C19.7573 6.27809 17.7396 4.25151 15.1038 3.97887C14.0768 3.8789 13.0407 3.82438 12.0045 3.82438Z" fill="#98208E"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10.7117 7.73941C9.84223 7.64884 8.96203 7.64884 8.09253 7.73938C6.54199 7.89947 5.30164 9.13956 5.14142 10.69C5.05085 11.5594 5.05084 12.4395 5.1414 13.3089C5.30147 14.8591 6.54133 16.0991 8.09145 16.2597C8.52796 16.3069 8.96665 16.3285 9.40376 16.3285C9.84067 16.3285 10.2794 16.3069 10.7127 16.2596C12.2627 16.099 13.5024 14.8592 13.6626 13.3092C13.7532 12.4397 13.7532 11.5596 13.6626 10.6902C13.5025 9.13973 12.2622 7.89959 10.7117 7.73941ZM8.2673 9.42658C7.49783 9.50713 6.90868 10.0966 6.82857 10.866C6.75203 11.6183 6.75203 12.3843 6.82857 13.1366C6.90878 13.9071 7.4955 14.496 8.26818 14.5761C9.02064 14.6527 9.78674 14.6527 10.5392 14.5762C11.3098 14.4959 11.8988 13.9092 11.9789 13.1367C12.0555 12.3844 12.0555 11.6184 11.979 10.866C11.8988 10.0959 11.3125 9.50711 10.5403 9.42659C10.1624 9.38985 9.78244 9.36995 9.40376 9.36995C9.02511 9.36995 8.6452 9.38985 8.2673 9.42658Z" fill="#B854AF"/>
</svg>
`
export function detectOperator(number, op = ''){
    if(number){
        let prefix = number.toString().slice(2,4)
        const firstTwoChar = number.slice(0,2)
        if(firstTwoChar == '09'){
            if(prefix === '10' || prefix === '11' || prefix === '12' || prefix === '13' || prefix === '14' || prefix === '15' || prefix === '16' || prefix === '17' || prefix === '18' || prefix === '19' || prefix === '90' || prefix === '91'){
                return {description:'همراه‌اول',code:'Mci', svg: hamrahaval}
            }else if(prefix === '01' || prefix === '02' || prefix === '03' || prefix === '05' || prefix === '30' || prefix === '33' || prefix === '35' || prefix === '36' || prefix === '37' || prefix === '38' || prefix === '39'){
                return {description:'ایرانسل', code:'Mtn', svg: irancell};
            }else if(prefix === '20' || prefix === '21' || prefix === '22'){
                return { description:'رایتل', code:'Rightel', svg: rightel}
            }
        }
        if(number == '00000053'){
            return {description:'ایرانسل', code:'Mtn', svg: irancell};
        }else if(number == '00000061'){
            return {description:'همراه‌اول',code:'Mci', svg: hamrahaval}
        }else if(number == '8921992'){
            return { description:'رایتل', code:'Rightel', svg: rightel}
        }
    }
    if(op){
        if(op == 'Mtn'){
            return {description:'ایرانسل', code:'Mtn', svg: irancell};
        }else if(op == 'Mci'){
            return {description:'همراه‌اول',code:'Mci', svg: hamrahaval}
        }else if(op == 'Rightel'){
            return { description:'رایتل', code:'Rightel', svg: rightel}
        }
        return {description:'', code:'', svg: null}
    }
    return {description:'', code:'', svg: null}

}
export function makeTable(data){
    if(data.type == 'topup'){
        return makeTopupTable(data)
    }else if(data.type == 'internet'){
        return makeInternetTable(data)
    }else if(data.type == 'chargeCode'){
        return makeChargeCodeTable(data)
    }
    return;
}
export function makeChargeCodeTable(table){
    const { data } = table;
    const arr = [];
    if(table.amount){
        arr.push({
            key: 'مبلغ هر کارت شارژ',
            value: `${persianNumber(numberSeperator(rialToToman(table.amount)))} تومان`,
        }) 
    }
    if(table.amount){
        arr.push({
            key: 'مبلغ کل',
            value: `${persianNumber(numberSeperator(rialToToman(table.amount * data.count)))} تومان`,
        }) 
    }
    // if(table.discount){
    //     arr.push({
    //         key: 'مبلغ با تخفیف',
    //         value: `${persianNumber(numberSeperator(rialToToman(table.amount * (100 - table.discount)/100)))} تومان`,
    //     }) 
    //     arr.push({
    //         key: 'میزان تخفیف',
    //         value: `${persianNumber(table.discount)} درصد`,
    //     }) 
    // }
    for(let item in data){
        if(item === 'isid'){
            arr.push({
                key: 'اپراتور',
                value: `${detectOperator(data.isid).description}`,
            }) 
        }
        if(item === 'vtpcode'){
            arr.push({
                key: 'کد کارت شارژ',
                value: persianNumber(data.vtpcode),
            }) 
        }
        if(item === 'count'){
            arr.push({
                key: 'تعداد کارت شارژ',
                value: persianNumber(data.count),
            }) 
        }
    }
    
    return arr
}
export function makeInternetTable(table){
    const { data, discount } = table;
    const arr = [];
    if(table.package_name){
        arr.push({
            key: 'نام بسته',
            value: `${table.package_name}`,
        }) 
    }
    
    for(let item in data){
        if(item === 'number'){
            arr.push({
                key: 'شماره موبایل',
                value: persianNumber(data.number),
            }) 
        }
        if(item === 'op'){
            arr.push({
                key: 'اپراتور',
                value: detectOperator('',data.op).description,
            }) 
        }
        
        if(item === 'simtype'){
            arr.push({
                key: 'نوع سیمکارت',
                value: getSimNamePersian(data.op, data.simtype),
            }) 
        }
        
    }
    if(table.price){
        arr.push({
            key: 'قیمت بسته بدون مالیات',
            value: `${persianNumber(numberSeperator(rialToToman(table.price)))} تومان`,
        }) 
    }
    if(table.finalPrice !== table.price ){
        arr.push({
            key: 'قیمت بسته با مالیات',
            value: `${persianNumber(numberSeperator(rialToToman(table.finalPrice)))} تومان`,
        }) 
    }
    if(table.discount){
        arr.push({
            key: 'مبلغ با تخفیف',
            value: `${persianNumber(numberSeperator(rialToToman(table.finalPrice * (100 - table.discount)/100)))} تومان`,
        }) 
        arr.push({
            key: 'میزان تخفیف',
            value: `${persianNumber(table.discount)} درصد`,
        }) 
    }
    
    return arr
}
export function makeTopupTable(table){
    const { data, discount } = table;
    const arr = [];
    for(let item in data){
        if(item === 'number'){
            arr.push({
                key: 'شماره موبایل',
                value: persianNumber(data.number),
            }) 
        }
        if(item === 'op'){
            arr.push({
                key: 'اپراتور',
                value: detectOperator('', data.op).description,
            }) 
        }
        if(item === 'amount'){
            arr.push({
                key: 'مبلغ شارژ',
                value: `${persianNumber(numberSeperator(rialToToman(data.amount)))} تومان`,
            }) 
        }
        
        if(item === 'simtype'){
            arr.push({
                key: 'نوع سیمکارت',
                value: getSimTypeDescription(data.simtype),
            }) 
        }
        if(item === 'profile'){
            arr.push({
                key: 'نوع شارژ',
                value: getChargeTypeDescription(data),
            }) 
        }
    }
    if(table.discount){
        arr.push({
            key: 'مبلغ با تخفیف',
            value: `${persianNumber(numberSeperator(rialToToman(data.amount * (100 - table.discount)/100)))} تومان`,
        }) 
        arr.push({
            key: 'میزان تخفیف',
            value: `${persianNumber(table.discount)} درصد`,
        }) 
    }
    
    return arr
}
export function getChargeTypeDescription(data){
    const { op, profile } = data
    const irancellList = [
        {value: 'direct',description: 'معمولی'},
        {value: 'extra',description: 'شگفت‌انگیز'}
    ];
    const mciList = [
        {value: 'direct',description: 'معمولی'},
        {value: 'lady',description: 'بانوان'},
        {value: 'youth',description: 'جوانان'},
        {value: 'desire',description: 'فوق‌العاده'},
    ];
    const rightelList = [
        {value: 'direct',description: 'معمولی'},
        {value: 'extra',description: 'شورانگیز'}
    ];
    if(op == 'Mci'){
        return mciList.find(type => type.value == profile).description;
    }
    if(op == 'Mtn'){
        return irancellList.find(type => type.value == profile).description;
    }
    if(op == 'Rightel'){
        return rightelList.find(type => type.value == profile).description;
    }
}
export function getSimTypeDescription(search){
    const simtypeList = [
        {value: 1 ,description: 'سیم‌کارت دائمی'},
        {value: 0 ,description: 'سیم‌کارت اعتباری'}
    ]
    return simtypeList.find(type => type.value == search).description;
}
export function getSimNamePersian(operator, search = 'all'){
    const simtypesList = {
        Mci: [
          {description: 'دائمی', value: '0'},
          {description: 'اعتباری', value: '1'},
          {description: 'دائمی و اعتباری', value: '2'},
          {description: 'همه سیم‌کارت‌ها', value: 'all'},
        ],
        Rightel: [
          {description: 'اعتباری', value: '1'},
          {description: 'دائمی', value: '2'},
          {description: 'همه سیم‌کارت‌ها', value: 'all'},
        ],
        Mtn: [
            {description: 'اعتباری', value: '1'},
            {description: 'دائمی', value: '2'},
            {description: 'TD_LTE اعتباری', value: '3'},
            {description: 'TD_LTE + FDD اعتباری', value: '4'},
            {description: 'اینترنت ثابت  ایرانسل', value: '5'},
            {description: 'مناسبتی', value: '6'},
            {description: 'TD_LTE دائمی', value: '7'},
            {description: 'TD_LTE + FDD دائمی', value: '8'},
            {description: 'همه سیم‌کارت‌ها', value: 'all'},
        ]
    }
    return simtypesList[operator].find(type => type.value == search).description;
}
  
export function isEmptyObject(obj){
    for(let item in obj){
        if(typeof obj[item] == 'undefined'){
            return true
        }
    }
    return false;
}
export function persianNumber(number){
    const id = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    if (number) {
      number = number.toString();
      return number.replace(
        /['0','1','2','3','4','5','6','7','8','9']/g,
        function(w) {
          return id[w];
        }
      );
    }
}
export function persianNumberToEnglish(number){
    const id = ['0','1','2','3','4','5','6','7','8','9'];
    if (number) {
      number = number.toString();
      return number.replace(
        /["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"]/g,
        function(w) {
          return id[persianNumber(w)];
        }
      );
    }
}
export function numberSeperator(number){
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "٬");
}
export function rialToToman(number){
    return Math.floor(number/10);
}
export function getCookie(name) {
    const v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}
export function setCookie(name, value, days) {
    const d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toUTCString();
}
export function deleteCookie(name) { setCookie(name, '', -1); }
export function clearFlatObject(obj){
    for(let i in obj){
        obj[i] = ''
    }
    return obj;
}
export function persianInternetPackage(name){
    switch(name){
        case 'annual':
            return 'سالانه';
        case 'daily':
            return 'روزانه';
        case 'fourmonths':
            return 'چهار ماهه';
        case 'monthly':
            return 'ماهانه'
        case 'nightly':
            return 'شبانه';
        case 'semiannual':
            return 'شش ماهه';
        case 'trimester':
            return 'سه ماهه';
        case 'twomonths':
            return 'دو ماهه';
        case 'weekly':
            return 'هفتگی';
        case 'threeDays':
            return 'سه‌روزه';
        case 'fifteenDays':
            return '۱۵ روزه';
        case 'hourly':
            return 'ساعتی';
        case 'weekly':
            return 'هفتگی';
        case 'reason':
            return 'reason';
        case 'other':
            return 'بسته‌های دیکر';
        default:
            return 'همه'
    }
}
export function getPackageTimeId(name){
    switch(name){
        case 'annual':
            return 365;
        case 'daily':
            return 1;
        case 'fourmonths':
            return 120;
        case 'monthly':
            return 30
        case 'nightly':
            return 1;
        case 'semiannual':
            return 180;
        case 'trimester':
            return 90;
        case 'twomonths':
            return 60;
        case 'weekly':
            return 7
    }
}


export function addToFavoritePhoneNumber(phoneNumber){
    let numbers = JSON.parse(localStorage.getItem('favoriteItems')) || [];
    if(phoneNumber){
        numbers.push(phoneNumber);
    }
    numbers = numbers.filter(function(item, pos) {
        return numbers.indexOf(item) == pos;
    })
    localStorage.setItem('favoriteItems', JSON.stringify(numbers))
}

export function getFavoritePhoneNumber(){
    let numbers = JSON.parse(localStorage.getItem('favoriteItems')) || [];
    numbers = numbers.filter(num => num)
    localStorage.setItem('favoriteItems', JSON.stringify(numbers))
    return numbers;
}

export function addToTransactions(transaction){
    let transactions = JSON.parse(localStorage.getItem('dHJhbnNhY3Rpb25z')) || [];
    if(transaction){
        transactions.push(transaction);
    }
    localStorage.setItem('dHJhbnNhY3Rpb25z', JSON.stringify(transactions))
}

export function getTransactions(){
    let transactions = JSON.parse(localStorage.getItem('dHJhbnNhY3Rpb25z')) || [];
    transactions = transactions.filter(transaction => transaction)
    localStorage.setItem('dHJhbnNhY3Rpb25z', JSON.stringify(transactions))
    return transactions;
}

export function getPathFromUrl(url) {
    console.log(url.split(/[?#]/)[0])
    return url.split(/[?#]/)[0];
}

export function getDiscount(type, op, amount = 0){
    const guest = {
        Mtn: 1,
        Mci: 0,
        Rightel: 1
    };
    if(type == 13){
        return 0;
    }
    if(amount < 2000000){
        return guest[op]
    }else{
        return guest[op] ? 2 : 0
    }
}
export function removeDublicateChargeCodePrice(arr){
    const uniqueArray = arr.filter(function(item, pos) {
        return arr.indexOf(item) == pos;
    })
    return uniqueArray
}
export function getQueryStringValue (name, url = null) {  
    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get(name);
    return myParam ? myParam.replace(/ /g, '+') : null
}
export function debounce(func, wait, immediate = false) {
	let timeout;
	return function() {
		let context = this, args = arguments;
		let later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		let callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};
  
  // Would write the value of the QueryString-variable called name to the console  